import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractualComponent } from './interactual.component';

describe('InteractualComponent', () => {
  let component: InteractualComponent;
  let fixture: ComponentFixture<InteractualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteractualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteractualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
