import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PostsService} from '../posts.service';
@Component({
  selector: 'app-interactual',
  templateUrl: './interactual.component.html',
  styleUrls: ['./interactual.component.css']
})
export class InteractualComponent implements OnInit {

  language:any;
  requestId :any = 'G_VNcFr8s7I'; 

  is_English:boolean = true;
  is_Chinese:boolean = false;
  is_France:boolean = false;
  is_German:boolean = false;
  is_Russian:boolean = false;
  is_Italian:boolean = false;
  is_Spanish:boolean = false;
  constructor(public post:PostsService,private translate: TranslateService) {
    this.language = this.post.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','HOME_chineese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','HOME_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','HOME_italian');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','HOME_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','HOME_spanish');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','HOME_german');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','HOME_english');

    }
    else {
      this.switchLanguage('en','HOME_english');

    }

    translate.setDefaultLang('HOME_english');

   }
  ngOnInit() {
  }
  switchLanguage(language: string, address:string) {
    var element = document.getElementById("myDIV");

    if(element != null){
    if(language === 'en'){
      this.requestId = 'G_VNcFr8s7I';
      element.className = 'flag-icon flag-icon-gb';
    }
    else if(language === 'ch'){
      this.requestId = 'G_VNcFr8s7I';
      element.className = 'flag-icon flag-icon-cn';
    }
    else if(language === 'rus'){
      this.requestId = 'XAb3SYVHj84';
      element.className = 'flag-icon flag-icon-me';
    }
    else if(language === 'ita'){
      this.requestId = 'sPWhDh5U-KE';
      element.className = 'flag-icon flag-icon-it';
    }
    else if(language === 'spa'){
      this.requestId = 'GBZg0a7nxqc';
      element.className = 'flag-icon flag-icon-es';
    }
    else if(language === 'ger'){
      this.requestId = 'XAUQ5pFyvQo';
      element.className = 'flag-icon flag-icon-de';
    }
    else if(language === 'fr'){
      this.requestId = 'SP-56TbXGO0';
      element.className = 'flag-icon flag-icon-fr';
    }
    else if(language === 'fr'){
      element.className = 'flag-icon flag-icon-fr';
    }
  }
  //  element.classList.add("flag-icon flag-icon-cn");

    this.post.setLanguage(language);
    this.translate.use(address);
}
}
