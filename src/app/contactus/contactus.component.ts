import { Component, OnInit,AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements AfterViewInit,OnInit {
  fname:any = "";
  lname:any = "";
  email:any = "";
  phone:any = "";
  msg:any = "";
  language:any;
  constructor(public app: AppComponent,private postsService: PostsService,
    private router: Router, private _route: ActivatedRoute, private translate: TranslateService) { 

      window.scrollTo(0,0);
  
      // translate.setDefaultLang('contactus_english');
  
     }
     switchLanguage(language: string, address:string) {
      var element = document.getElementById("myDIV");
      if(element != null){
        if(language === 'en'){
          element.className = 'flag-icon flag-icon-gb';
        }
        else if(language === 'ch'){
          element.className = 'flag-icon flag-icon-cn';
        }
        else if(language === 'rus'){
          element.className = 'flag-icon flag-icon-me';
        }
        else if(language === 'ita'){
          element.className = 'flag-icon flag-icon-it';
        }
        else if(language === 'spa'){
          element.className = 'flag-icon flag-icon-es';
        }
        else if(language === 'ger'){
          element.className = 'flag-icon flag-icon-de';
        }
        else if(language === 'fr'){
          element.className = 'flag-icon flag-icon-fr';
        }
    
      }



      this.postsService.setLanguage(language);
      this.translate.use(address);
  }
  ngOnInit() {
    this.app.isAutherizes  = false;
  }
  ngAfterViewInit(){

    this.language = this.postsService.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','contactus_chines');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','CONTACT_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','itallian_contactus');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','CONTACT_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','spanish_contact_us');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','german__CONTACT US');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','contactus_english');

    }
    else {
      this.switchLanguage('en','contactus_english');

    }

  }
  getcontactus(){
    if (this.isEmail(this.email)) {
      if (this.isPhone(this.phone)) {
    let contact = {
      "firstName":this.fname,
      "lastName":this.lname,
      "email":this.email,
      "phonenumber":this.phone,
      "message":this.msg
    }
    if(this.fname === "" ||this.lname === "" || this.email ==="" || this.phone ==="" || this.msg ===""){
      alert("please enter valid details");
    }
    else{
      this.postsService.contact(contact)
      .subscribe(result => {
        if(result){
          alert(result.msg);
          this.fname = "";
          this.lname = "";
          this.email = "";
          this.phone = "";
          this.msg = "";
        }
        else{

        }
        
        
      })
    }
   
  }
  else {
    alert("Please enter valid Phone Number");
    
    this.phone = '';
  }
}


else {
  alert("Please enter valid Email Address");

this.email = '';
}
  }

  
  isPhone(inputtxt) {
    var phoneno = /^\d{10,14}$/;
    if ((inputtxt.match(phoneno))) {
        return true;
    }
    else {
        return false;
    }
}
isfullname(inputtxt) {
    var letters = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
    if ((inputtxt.match(letters))) {
        return true;
    }
    else {
        return false;
    }
}
isEmail(search: string) {

  var serchfind: boolean;
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  let regexp = new RegExp('^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');
  // alert("Please enter valid email")
  serchfind = re.test(search.toLowerCase());

  return serchfind


}

}
