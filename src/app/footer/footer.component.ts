import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public app: AppComponent,private translate: TranslateService) { 

    translate.setDefaultLang('aboutus_english');
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }


  ngOnInit() {

    // this.app.isAutherizes  = false;
  }

}

