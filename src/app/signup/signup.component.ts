import { Component, OnInit, ViewEncapsulation } from '@angular/core';

//import { Meta } from '@angular/platform-browser';
//import { Observable } from 'rxjs/Rx';
import { PostsService } from '../posts.service';
// import alertify from 'alertify.js';
//import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
declare var $:any;
export class RegisterData {

  fullname: string;
  username: string;
  email: string;
  password:string;
  reference:string;
  captcha:string;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    public aFormGroup: FormGroup;

    sitekey:string="6LdP_XMUAAAAAGe0RqcDIQ1shbIZ8Mln0jwhtyoZ"

repassword:string;
error: string;
returnUrl: string;
// captcha:any="";
reff: any = {};
url: any = {};
color: any = {};
disabled: boolean;
uservalidationData: any = {};
emailvalidationData: any = {};
reffervalidationData: any = {};
size:any = 'Normal';
    lang:any = 'en';
    theme:any = 'Light';
    type:any = 'Image';

model2: RegisterData = {

    fullname: "",
    username: "",
    email: "",
   
    password: "",
    reference: "",
    captcha:"",

}

constructor( private postsService: PostsService,
    private router: Router, private _route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public app: AppComponent
   ) {

    if(document.getElementById('mobile-nav-toggle')!= undefined){
        var link = document.getElementById('mobile-nav-toggle');
        link.style.display = 'none';
        $("nav").hide();
    }

    // this.returnUrl = this._route.snapshot.queryParams['reff'] || '/';

    // this.reff = this.returnUrl.split("=")[0];


    // if (this.reff !== "/") {

    //     this.model2.reference = this.reff;
    //     this.disabled = true;

    // } else {

    //     this.model2.reference = "";
    //     this.disabled = false;
    // }

}
ngOnInit() {
    this.app.isAutherizes  = false;
    this.aFormGroup = this.formBuilder.group({
        recaptcha: ['', Validators.required]
      });
    let cookielist = Cookie.getAll();
    if (cookielist.referenceAlphaX != null || cookielist.referenceAlphaX != '' || cookielist.referenceAlphaX != undefined) {
        this.model2.reference = cookielist.referenceAlphaX;
    }
    else{
    this.model2.reference = "";
    }
}


//copyURL() {
//
//    this.parents(".inputTarget").children("input").select();
//    if (document.execCommand('copy')) {
//        alertify.alert("Link copied successfully.");
//
//    } else {
//        alertify.alert(" Copy link failed, please try again.");
//
//    }
//}

uservalidation() {

    if (this.model2.username.includes(" ")) {
        this.model2.username = this.model2.username.split(" ")[0]
         alert("No space allowed");
          

    } else {

        this.uservalidationData = {


            username: this.model2.username

        }

        this.postsService.checkuserName(this.uservalidationData)
            .subscribe(
            data => {
                if (data) {
                    if (data.status === "ERROR") {

                         alert(data.msg);
                       
                        this.model2.username = "";

                    }
                }

            });
    }
}

reffer() {
    if (this.model2.reference.includes(" ")) {
        this.model2.reference = this.model2.reference.split(" ")[0]
         alert("No space allowed in reference");
    } else if (this.model2.username == this.model2.reference) {
         alert("Please check your Reference");
         this.model2.reference = "";
    }
}


Emailvalidation() {

    this.emailvalidationData = {


        email: this.model2.email

    }
    this.postsService.checkEmail(this.emailvalidationData)
        .subscribe(
        data => {
            if (data) {
                if (data.status === "ERROR") {

                     alert(data.msg);
                   
                    this.model2.email = "";

                }
            }

        });

}

signup() {
    
        if (this.model2.fullname == "" || this.model2.username == "" || this.model2.email == "" ||  this.model2.password == "") {

            alert("Please Enter The Valid Details");
            grecaptcha.reset()
        }
        // else if (this.model2.reference !== "") {
        //
        //    this.reffervalidationData = {
        //
        //
        //        username: this.model2.reference
        //
        //    }
        //
        //    this.postsService.checkuserName(this.reffervalidationData)
        //        .subscribe(
        //        data => {
        //            if (data) {
        //                if (data.status === "OK") {
        //
        //                    alert("Referral Not Found! please check your Referral");
        //                    this.model2.reference = "";
        //
        //                } else {
        //
        //
        //                    this.model2.username = this.model2.username.trim();
        //                    this.model2.fullname = this.model2.fullname.trim();
        //                    this.model2.email = this.model2.email.trim();
        //                    this.model2.phone = this.model2.phone.trim();
        //                    this.model2.password = this.model2.password.trim();
        //
        //                    if (this.model2.username.includes(" ")) {
        //                        this.model2.username = this.model2.username.split(" ")[0]
        //                    }
        //
        //
        //                    this.postsService.Registration(this.model2)
        //                        .subscribe(
        //                        data => {
        //
        //                            if (data) {
        //                                if (data.status === "OK") {
        //
        //                                    alert("Thank you. To complete your registration please check your inbox/spam.");
        //                                    this.router.navigate(['/login']);
        //                                }
        //                                else if (data.status === "ERROR") {
        //                                    alert(data.msg);
        //                                    // this._alertService.error('Please enter valid details. ' +  data.msg);
        //                                }
        //                            } else {
        //                                alert("Error occurred please try again. " + data.msg);
        //                                // this.error = 'Error occurred please try again.'+ data.msg;
        //                            }
        //
        //                        });
        //
        //
        //
        //                }
        //            }
        //
        //        });
        //
        //
        //}

        else {
            if(this.isfullname(this.model2.fullname)){
                if(this.isEmail(this.model2.email)){

                
                // this.router.navigate(['/selectCitizen']);
                this.postsService.Registration(this.model2)
                    .subscribe(
                    data => {
    
                        if (data) {
                            if (data.status === "OK") {
                                let key = 'userName';
                                localStorage.setItem(key, this.model2.username);
    
    
                                localStorage.setItem('isCitizenship', 'true');
    
                                alert("Thank you. To complete your registration please check your mail inbox/spam.");
                                this.router.navigate(['/login']);
                            }
                            else if (data.status === "ERROR") {
                                alert(data.msg);
                                grecaptcha.reset()
                            }
                        } else {
                            alert("Error occurred please try again. " + data.msg);
                            grecaptcha.reset()
                        }
    
                    });
            } else{
                alert('Please enter valid email');
            }
        } else{
            alert('Please enter valid full name');
        }
          


        }
    // } else {
    //     alert('Please enter the required details');
    // }
}

isEmail(search: string): boolean {
    var serchfind: boolean;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let regexp = new RegExp('^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');

    serchfind = re.test(search.toLowerCase());
    return serchfind
}

isfullname(inputtxt) {
    var letters = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
    if ((inputtxt.match(letters))) {
        return true;
    }
    else {
        return false;
    }
}
handleSuccess(event){
    this.model2.captcha = event;

}
handleExpire(){}
handleLoad(){}
}
