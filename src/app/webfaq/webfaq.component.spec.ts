import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebfaqComponent } from './webfaq.component';

describe('WebfaqComponent', () => {
  let component: WebfaqComponent;
  let fixture: ComponentFixture<WebfaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebfaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebfaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
