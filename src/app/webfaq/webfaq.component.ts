import { Component, OnInit,AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { TranslateService } from '@ngx-translate/core';
import { PostsService} from '../posts.service';
@Component({
  selector: 'app-webfaq',
  templateUrl: './webfaq.component.html',
  styleUrls: ['./webfaq.component.css']
})
export class WebfaqComponent implements AfterViewInit,OnInit {

  language:any;

  constructor(public post:PostsService, public app: AppComponent,private translate: TranslateService) {
    
    window.scrollTo(0,0);

    // translate.setDefaultLang('FAQ_english');

   }
   switchLanguage(language: string, address:string) {
    var element = document.getElementById("myDIV");
    if(element != null){
      if(language === 'en'){
        element.className = 'flag-icon flag-icon-gb';
      }
      else if(language === 'ch'){
        element.className = 'flag-icon flag-icon-cn';
      }
      else if(language === 'rus'){
        element.className = 'flag-icon flag-icon-me';
      }
      else if(language === 'ita'){
        element.className = 'flag-icon flag-icon-it';
      }
      else if(language === 'spa'){
        element.className = 'flag-icon flag-icon-es';
      }
      else if(language === 'ger'){
        element.className = 'flag-icon flag-icon-de';
      }
      else if(language === 'fr'){
        element.className = 'flag-icon flag-icon-fr';
      }
  
    }
    this.post.setLanguage(language);
    this.translate.use(address);
}
  ngOnInit() {
    this.app.isAutherizes  = false;
    
  }
  ngAfterViewInit(){
     
    this.language = this.post.baseLanguage;
    // console.log("webfaq" +this.language);
    if(this.language === "ch"){
      this.switchLanguage('ch','FAQ_chineese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','FAQ_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','FAQ_italian');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','FAQ_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','FAQ_spanish');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','FAQ_german');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','FAQ_english');

    }
    else {
      this.switchLanguage('en','FAQ_english');

    }
  }
}
