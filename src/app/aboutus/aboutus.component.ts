import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import {TranslateService} from '@ngx-translate/core';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements AfterViewInit,OnInit {

  language:any;
  is_English:boolean = true;
  is_Chinese:boolean = false;
  is_France:boolean = false;
  is_German:boolean = false;
  is_Russian:boolean = false;
  is_Italian:boolean = false;
  is_Spanish:boolean = false;
  requestId :any = 'G_VNcFr8s7I'; 

  constructor(public app: AppComponent,private translate: TranslateService,private postsService: PostsService,
    private router: Router, private _route: ActivatedRoute,) { 
        // translate.setDefaultLang('aboutus_english');
        window.scrollTo(0,0);
   }
   switchLanguage(language: string, address:string) {
    var element = document.getElementById("myDIV");

    if(element != null){
    if(language === 'en'){
      this.requestId = 'G_VNcFr8s7I';
      element.className = 'flag-icon flag-icon-gb';
    }
    else if(language === 'ch'){
      this.requestId = 'S28aZw_1igU';
      element.className = 'flag-icon flag-icon-cn';
    }
    else if(language === 'rus'){
      this.requestId = 'XAb3SYVHj84';
      element.className = 'flag-icon flag-icon-me';
    }
    else if(language === 'ita'){
      this.requestId = 'sPWhDh5U-KE';
      element.className = 'flag-icon flag-icon-it';
    }
    else if(language === 'spa'){
      // this.requestId = 'GBZg0a7nxqc';
      this.requestId = 'tOn75Zt1Cqk';
      element.className = 'flag-icon flag-icon-es';
    }
    else if(language === 'ger'){
      this.requestId = 'XAUQ5pFyvQo';
      element.className = 'flag-icon flag-icon-de';
    }
    else if(language === 'fr'){
      this.requestId = 'SP-56TbXGO0';
      element.className = 'flag-icon flag-icon-fr';
    }
    else if(language === 'fr'){
      // this.src = "https://www.youtube.com/embed/tOn75Zt1Cqk";
      element.className = 'flag-icon flag-icon-fr';
    }
  }
  //  element.classList.add("flag-icon flag-icon-cn");

    this.postsService.setLanguage(language);
    this.translate.use(address);
}
   

  ngOnInit() {
    this.app.isAutherizes  = false;
  }
  ngAfterViewInit() {
    this.language = this.postsService.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','aboutus_chinese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','ABOUTUS_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','aboutus_italian');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','ABOUTUS_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','spanish_about_us');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','german_about_us');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','aboutus_english');

    }
    else {
      this.switchLanguage('en','aboutus_english');

    }

  }

}
