import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { NgxSpinnerService } from 'ngx-spinner';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
// import {Observable} from 'rxjs/Observable';
// import 'rxjs/add/observable/fromEvent';
import {Observable, fromEvent} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = '';
  returnUrl: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  isRouteLogin : boolean = false;
  public isAutherizes: boolean = false;

  constructor(private router: Router, private _route: ActivatedRoute,
              private spinner: NgxSpinnerService, 
              private idle: Idle, private keepalive: Keepalive
              ) {
    let route: any = '';
    route = window.location.href.split("/");
    
    setTimeout(function () {
      route = window.location.href.split("/");
      if (route[4] === 'login' ||
        route[4] === 'signup' ||
        route[4] === '2fa' ||
        route[4] === 'forgotpassword' ||
        route[4] === 'securitycode' ||
        route[4] === 'changepassword' ||
        route[4] === 'home' ||
        route[4] === 'aboutus' ||
        route[4] === 'team' ||
        route[4] === 'webfaq' ||
        route[4] === 'contactus') {
        this.isAutherizes = false;
      }
      else {
        this.isAutherizes = true;
      }
    }, 1000);

    this.router.events.subscribe((res) => {
      if( this.router.url == '/login'){
        this.isRouteLogin = true;
      }
      else{
        this.isRouteLogin = false;
      }
      
    })


     // sets an idle timeout of 5 seconds, for testing purposes.
     idle.setIdle(150);
     // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
     idle.setTimeout(150);
     // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
     idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
 
     idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
     idle.onTimeout.subscribe(() => {
       this.idleState = 'Timed out!';
       this.timedOut = true;
       
       if(JSON.parse(localStorage.getItem('currentUserAlphaX')) != null ){
       localStorage.removeItem('currentUserAlphaX');
       this.router.navigate(['/login']);
       alert("Your session has been expired ! Please refresh your page and login again.");
       }
       
      });
      
     idle.onIdleStart.subscribe(() => {
       
      this.idleState = 'You\'ve gone idle!'
      
    });
     idle.onTimeoutWarning.subscribe((countdown) => {
       this.idleState = 'You will time out in ' + countdown + ' seconds!'
       
       
    });
     keepalive.interval(15);
     keepalive.onPing.subscribe(() => {
      this.lastPing = new Date()
      
    });
 
     this.reset();
  
  }
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
  }, 2000);
    let route: any = '';
    route = window.location.href.split("/");
    // let referalToken = route[4];
    let string = route[4]
    if(string.includes("?")){
      let value = string.split("=");
      let cookie = value[1];
      Cookie.set('referenceAlphaX', cookie , 10);
      this.router.navigate(['/login']);
    }
    // if(route[4].include("?")){
    //   let arrayReferal = route[4].split("?");
    // }
    // else{
    // }

    let token = route[5].split("=")[1];
            if (token != undefined || token != '') {
              localStorage.setItem("activateAlpaxAccount", token);
              this.router.navigate(['/login']);
            }
    setTimeout(function () {
      route = window.location.href.split("/");
      if (route[4] === 'login' ||
        route[4] === 'signup' ||
        route[4] === '2fa' ||
        route[4] === 'forgotpassword' ||
        route[4] === 'securitycode' ||
        route[4] === 'changepassword' ||
        route[4] === 'home' ||
        route[4] === 'aboutus' ||
        route[4] === 'team' ||
        route[4] === 'webfaq' ||
        route[4] === 'contactus') {
        this.isAutherizes = false;
            // let token = route[5].split("=")[1];
            // if (token != undefined || token != '') {
            //   localStorage.setItem("activateAlpaxAccount", token);
            //   this.router.navigate(['/login']);
            // }
      }
      else {
        this.isAutherizes = true;
      }
    }, 1000)
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  
}
