import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransactionComponent } from './transaction/transaction.component';
import { FaqComponent } from './faq/faq.component';
import { ProfileComponent } from './profile/profile.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AsidenavComponent } from './asidenav/asidenav.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { PostsService } from './posts.service';
import { TrancComponent } from './transaction/tranc/tranc.component';
import { HttpClientModule, HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { PasswordchangeComponent } from '../app/passwordchange/passwordchange.component';
import { TwofaComponent } from '../app/twofa/twofa.component';
import { SecuritycodeComponent } from '../app/securitycode/securitycode.component';
import { ClipboardModule } from 'ngx-clipboard';
import { DatePipe } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { AuthGuard } from './guard.service';
import { AffiliateComponent } from './affiliate/affiliate.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TeamComponent } from './team/team.component';
import { WebfaqComponent } from './webfaq/webfaq.component';
import { ContactusComponent } from './contactus/contactus.component';

import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { NgxSpinnerModule } from 'ngx-spinner';
import { ExchangeComponent } from './exchange/exchange.component';
import { PaymentComponent } from './payment/payment.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SafePipe } from './safe.pipe';
import { LegalInfoComponent } from './legal-info/legal-info.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { InteractualComponent } from './interactual/interactual.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { CoinSaleComponent } from './coin-sale/coin-sale.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NewsComponent } from './news/news.component';
import { DisclaimerWhitepaperComponent } from './disclaimer-whitepaper/disclaimer-whitepaper.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    DashboardComponent,
    TransactionComponent,
    FaqComponent,
    ProfileComponent,
    HeaderComponent,
    FooterComponent,
    AsidenavComponent,
    LoginComponent,
    SignupComponent,
    ForgetpasswordComponent,
    TrancComponent,
    PasswordchangeComponent,
    TwofaComponent,
    SecuritycodeComponent,
    AffiliateComponent,
    HomeComponent,
    AboutusComponent,
    TeamComponent,
    WebfaqComponent,
    ContactusComponent,
    ExchangeComponent,
    PaymentComponent,
    SafePipe,
    LegalInfoComponent,
    PrivacyPolicyComponent,
    InteractualComponent,
    TermsConditionsComponent,
    DisclaimerComponent,
    CoinSaleComponent,
    NewsComponent,
    DisclaimerWhitepaperComponent
    
    
  ],
  imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        ClipboardModule,
        NgxPaginationModule,
        IonRangeSliderModule,
        NgxSpinnerModule,
        NgxCaptchaModule,
        RecaptchaModule.forRoot(),
        Ng2CarouselamosModule,
        NgIdleKeepaliveModule.forRoot(),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        // BootstrapModalModule.forRoot({ container: document.body }),
    RouterModule.forRoot([])
  ],
  exports: [
    RouterModule,
], 
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },PostsService,DatePipe,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
