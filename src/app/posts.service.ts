import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { Response } from '@angular/http';
//import { Http,Headers, RequestOptions } from '@angular/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';


@Injectable()
export class PostsService {
    private userName: string;
    user: any;
    headers: any;
    idleState = 'Not started.';
    timedOut = false;
    lastPing?: Date = null;
    isRouteLogin: boolean = false;
    public isAutherizes: boolean = false;
    constructor(private http: Http, private router: Router, private httpClient: HttpClient,
        private idle: Idle, private keepalive: Keepalive) {
        this.setHeader();
    }

    setHeader() {
        this.user = JSON.parse(localStorage.getItem('currentUserAlphaX'));

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        if (this.user != null) {
            this.headers.append('Authorization', "Bearer " + this.user.token);
        }
    }
    setHeaderForBuy() {
        this.user = JSON.parse(localStorage.getItem('currentUserAlphaX'));

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        if (this.user != null) {
            this.headers.append('Authorization', "Bearer " + this.user.token);
        }
    }
    // Get all posts from the API
    getAllPosts() {
        return this.http.get('/api/posts')
            .map(res => res.json());
    }

    //public baseUrl = 'http://18.130.172.178:29293';
    public baseUrl = 'https://alpha-x.io:29293';
    // public baseUrl = 'http://localhost:3000';
   // public baseUrl = 'http://35.177.92.133:29293';

    // public localURL = 'http://localhost:4200';
    public localURL = 'https://alpha-x.io'; 



    private GETLiveUrl = this.baseUrl + '/liveValue';
    private getBalances = this.baseUrl + '/buyTransaction/getbalances';
    private GETdashboardUrl = this.baseUrl + '/getbalances';

    public baseLanguage = "EN";

    getLive() {

        return this.http.get(this.GETLiveUrl) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    //ICO details code
    // icodetails() {

    //     return this.http.get(this.icodetailsUrl) // ...using post request
    //         .map(res => res.json()) // ...and calling .json() on the response to return data
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    // }

    getBalance(): Promise<any> {

        return this.http.post(this.getBalances, { "userName": this.user.userName }, { headers: this.headers }) // ...using post request
            .map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    // get2FaQrCodeAddress(body: Object): Observable<any> {

    //     let bodyString = JSON.stringify(body); // Stringify payload
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     let opts = new RequestOptions();


    //     return this.http.post(this.get2FaQrCodeUrl, JSON.stringify(body), {

    //         headers: headers
    //     }).map(res => res.json()) // ...and calling .json() on the response to return data
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    // }

    // get2fastatusApi(body: Object): Observable<any> {

    //     let bodyString = JSON.stringify(body); // Stringify payload
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     let opts = new RequestOptions();


    //     return this.http.post(this.get2fastatusUrl, JSON.stringify(body), {

    //         headers: headers
    //     }).map(res => res.json()) // ...and calling .json() on the response to return data
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    // }
    // validateandEnableApi(body: Object): Observable<any> {
    //     let bodyString = JSON.stringify(body);
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     let opts = new RequestOptions();
    //     return this.http.post(this.validateandEnableUrl, JSON.stringify(body), {
    //         headers: headers
    //     }).map(res => res.json())
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    // }
    // public liveUrlFrontEnd = this.baseUrl;
    private RegisterUrl = this.baseUrl + '/register';
    private checkUsernameUrl = this.baseUrl + '/auth/checkUsername';
    private checkEmailUrl = this.baseUrl + '/auth/checkEmail';
    private LoginUrl = this.baseUrl + '/login';
    private UpdateBTCtrans = this.baseUrl + '/updateBTCtrans';
    private UpdateETHtrans = this.baseUrl + '/updateETHtrans';
    private UpdateLTCtransURL = this.baseUrl + '/updateLTCtrans';
    private activateAccountUrl = this.baseUrl + '/auth/activateAccount';
    private ForgotUrl = this.baseUrl + '/auth/forgotpassword';
    private changepass = this.baseUrl + '/auth/changepass';
    private resetpasstUrl = this.baseUrl + '/auth/resetpass';
    private ValidateTokenUrl = this.baseUrl + '/auth/validateToken';
    private getBTCTransactionURL = this.baseUrl + '/auth/getBTCtransaction';
    private getETHTransactionURL = this.baseUrl + '/auth/getETHtransaction';
    private getLTCTransactionURL = this.baseUrl + '/auth/getLTCtransaction';
    private getBCHTransactionURL = this.baseUrl + '/auth/getBCHtransaction';

    private getUserProfileUrl = this.baseUrl + '/auth/getuserprofile';
    private updateUserDetailURL = this.baseUrl + '/auth/UserDetaildsUpdate';
    private get2FaQrCodeUrl = this.baseUrl + '/enable2faGetqrcode';
    private get2fastatusUrl = this.baseUrl + '/get2fastatus';
    private validateandEnableUrl = this.baseUrl + '/validateandEnable';
    private verifytoken = this.baseUrl + '/enable2faValidatetoken';
    private getReferalsUrl = this.baseUrl + '/auth/getReferals';
    private getBtcAddressURL = this.baseUrl + '/auth/getbtcaddress';
    private getEthAddressURL = this.baseUrl + '/auth/getethaddress';
    private getBchAddressURL = this.baseUrl + '/auth/getbchaddress';
    private getLtcAddressURL = this.baseUrl + '/auth/getltcaddress';
    private getAlphaXAddressURL = this.baseUrl + '/auth/getalphaxaddress';

    private getAllBalancesURL = this.baseUrl + '/trans/getbalances';
    // private icodetailsUrl = this.backendUrl + '/auth/alphaxDetails';
    private EthPayoutAddress = this.baseUrl + '/auth/updateETHPayoutAddressbyusername';
    private getEthPayoutAddressURL = this.baseUrl + '/auth/getETHPayoutAddressbyusername';
    private buyWithETHURL = this.baseUrl + '/trans/buywitheth';
    private buyWithBTCURL = this.baseUrl + '/trans/buywithbtc';
    private submitKYCUrl = this.baseUrl + "/kyc/kyc_save";
    private kycstatusURL = this.baseUrl + "/kyc/kycstatus";
    private getequivalentUSD = this.baseUrl + "/trans/generatealphaxvalue";
    private getLocalTransactionURL = this.baseUrl + '/auth/getLocalTransactions';
    private withdrawORO = this.baseUrl + '/alpha_withdraw';
    private withdrawBTCUrl = this.baseUrl + '/bitcoinWithdraw';
    private UpdateBCHtrans = this.baseUrl + '/updateBCHtrans';
    private getSupportRequestbyUsernameUrl = this.baseUrl + "/getSupportRequestbyUsername";
    // private getSupportRequestbyUsernameUrl = this.baseUrl + "/getRequests";
    private createNewRequestUrl = this.baseUrl + "/create_request";
    private getSupportRequestbyIdUrl = this.baseUrl + "/getSupportRequestbyID";
    private submitUserReplyUrl = this.baseUrl + "/userreply";
    private BuyWithbchUrl = this.baseUrl + "/trans/buywithbch";
    private BuywithLtcUrl = this.baseUrl + "/trans/buywithltc";
    private BuywithEthUrl = this.baseUrl + "/trans/buywitheth";
    private BuywithBtcUrl = this.baseUrl + "/trans/buywithbtc";
    private generatealphaxvalueURL = this.baseUrl + "/trans/generatealphaxvalue";
    private soldtokenDetailURL = this.baseUrl + "/getsoldTokenDetails";
    private minimumBalanceURL = this.baseUrl + "/trans/alphaxminusdvalue";
    private withdrawURL = this.baseUrl + "/withdraw/AllRefWithdrawl";
    private getWithdrawdetailsUrl = this.baseUrl + "/withdraw/getwithdrawDetails";
    private contactUsURL = this.baseUrl + "/auth/contactUs";
    private getReferalToUsdURL = this.baseUrl + "/auth/getReferalToUsd";
    private getrequestToPayoutURL = this.baseUrl + "/withdraw/requestToPayout";
    private getpayoutAddressURL = this.baseUrl + "/withdraw/getBTCPayoutAddressbyusername";
    private updatepayoutAddressURL = this.baseUrl + "/withdraw/updateBTCPayoutAddressbyusername";
    private slabDate = this.baseUrl + "/auth/getProgressSlabValue";
    private newsDetails = this.baseUrl + "/getAllNews";
    private getUsercountURL = this.baseUrl + "/getUsercount";
    private generateAlphaxAddressURL = this.baseUrl + "/generateAlphaxAddressAfterLogin";
    private generateBTCAddressURL = this.baseUrl + "/generateBTCAddressAfterLogin";
    private generateETHAddressURL = this.baseUrl + "/generateETHAddressAfterLogin";
    private generateBCHAddressURL = this.baseUrl + "/generateBCHAddressAfterLogin";
    private generateLTCAddressURL = this.baseUrl + "/generateLTCAddressAfterLogin";

    Registration(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.RegisterUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }



    checkuserName(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.checkUsernameUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    checkEmail(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.checkEmailUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    Login(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.LoginUrl, JSON.stringify(body), {

            headers: headers
        }).map((response: Response) => {
            // login successful if there's a jwt token in the r
            // esponse
            let user = response.json();

            if (user.status != 'ERROR') {
                let tokendata = {

                    twofatoken: user.twoFAstatus,
                    userName: user.userName,

                }


                if (user.twoFAstatus) {

                    localStorage.setItem('twoFAstatus', JSON.stringify(tokendata));
                    return true;

                } else {

                    if (user.token) {

                        localStorage.setItem('currentUserAlphaX', JSON.stringify(user));
                        return true;
                    } else {


                        alert(user.msg);
                        this.router.navigate(['/login']);
                        return false;

                    }
                }
            }

            else {
                alert(user.msg);
                return false;
            }
        }).catch((error: any) => this.checkError(error));
    }



    //Account activation code
    activateCode(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.activateAccountUrl, JSON.stringify(body), {

            headers: headers
        }) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    TransferBTC(body: Object): Observable<any> {
        let bodyString = (body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();
        return this.http.post(this.withdrawBTCUrl, (body), {
            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    TransferORO(body: Object): Observable<any> {

        let bodyString = (body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();
        return this.http.post(this.withdrawORO, (body), {
            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error) => this.checkError(error)); //...errors if any
    }

    loginCheck(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.LoginUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()).catch((error: any) => this.checkError(error));
    }

    dashbordlivevalue(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();


        return this.http.post(this.GETdashboardUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    updateBTCtran(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.UpdateBTCtrans, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    forgotPassword(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.ForgotUrl, JSON.stringify(body), {

            headers: headers
        }) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    resetUserPassword(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.resetpasstUrl, JSON.stringify(body), {

            headers: headers
        }) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    // Forgot password verify code
    forgotVerifyCode(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.ValidateTokenUrl, JSON.stringify(body), {

            headers: headers
        }) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getBTCTransaction(body): Promise<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.getBTCTransactionURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getETHTransaction(body): Promise<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.getETHTransactionURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getLTCTransaction(body): Promise<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.getLTCTransactionURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getBCHTransaction(body): Promise<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.getBCHTransactionURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getLocalTransaction(body): Promise<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.getLocalTransactionURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getUserProfile(): Promise<any> {
        return this.http.post(this.getUserProfileUrl, { "userName": this.user.userName }, { headers: this.headers })
            .map(res => res.json()).toPromise()
            .catch((error: any) => this.checkError(error));

    }


    updateDetails(body): Observable<any> {

        let headers = new Headers();
        headers.append("Accept", "/*/");
        let opts = new RequestOptions();

        return this.http.post(this.updateUserDetailURL, (body), { headers: headers })
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }


    passwordchange(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();

        return this.http.post(this.changepass, JSON.stringify(body), { headers: this.headers })
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    get2FaQrCodeAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        this.setHeader();
        let opts = new RequestOptions();


        return this.http.post(this.get2FaQrCodeUrl, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    get2fastatusApi(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        this.setHeader();
        let opts = new RequestOptions();


        return this.http.post(this.get2fastatusUrl, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    validateandEnableApi(body: Object): Observable<any> {
        let bodyString = JSON.stringify(body);
        this.setHeader()
        let opts = new RequestOptions();
        return this.http.post(this.validateandEnableUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }

    updateETHtran(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.UpdateETHtrans, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json().toPromise()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    updateBCHtran(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.UpdateBCHtrans, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json().toPromise()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    updateLTCtran(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.UpdateLTCtransURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json().toPromise()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    verify2fa(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.verifytoken, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getReferals(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();


        return this.http.post(this.getReferalsUrl, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getalphaXAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getAlphaXAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    btcAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getBtcAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    ethAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getEthAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    bchAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getBchAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    ltcAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getLtcAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    getAllBalanceApi(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.getAllBalancesURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error) => { return this.checkError(error); });
    }
    postEthPayoutAddress(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.EthPayoutAddress, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    getEthPayoutAddress(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.getEthPayoutAddressURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    buyWithEth(body: Object): Observable<any> {
        this.setHeaderForBuy();
        return this.http.post(this.buyWithETHURL, (body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    buyWithBtc(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.buyWithBTCURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    submitKyc(body): Promise<any> {

        let headers = new Headers();
        headers.append('Accept', '/*/');
        let opts = new RequestOptions();
        return this.http.post(this.submitKYCUrl, (body), {
            headers: headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    kycAlreadyexist(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.kycstatusURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    getSupportRequest(body: Object): Promise<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.getSupportRequestbyUsernameUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getSupportRequestByID(body: Object): Promise<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.getSupportRequestbyIdUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    creatNewSupportRequest(body: Object): Promise<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.createNewRequestUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    submitUserReply(body: Object): Promise<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        let opts = new RequestOptions();
        return this.http.post(this.submitUserReplyUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json()).toPromise() // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    equivqlentUSD(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.getequivalentUSD, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    BuywithBTC(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.BuywithBtcUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    BuywithETH(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.BuywithEthUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    BuywithBCH(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.BuyWithbchUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    BuywithLTC(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.BuywithLtcUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    generatealphaxvalue(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.generatealphaxvalueURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    getsoldtokenDetails() {

        return this.http.get(this.soldtokenDetailURL) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getMinimumBalance(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.minimumBalanceURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    withdrawRequestApi(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.withdrawURL, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }
    withdrawdetails(body: Object): Observable<any> {
        this.setHeader();
        return this.http.post(this.getWithdrawdetailsUrl, JSON.stringify(body), {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));
    }

    contact(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();

        return this.http.post(this.contactUsURL, JSON.stringify(body), {

            headers: headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getReferalUSD(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getReferalToUsdURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getRequestPayout(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getrequestToPayoutURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    getBTCPayoutAddress(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.getpayoutAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }
    updatePayoutBTC(body: Object): Observable<any> {

        let bodyString = JSON.stringify(body); // Stringify payload
        this.setHeader();
        return this.http.post(this.updatepayoutAddressURL, JSON.stringify(body), {

            headers: this.headers
        }).map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

    setLanguage(lang) {
        this.baseLanguage = lang;
    }

    // getdateslab() {

    //     return this.http.get(this.slabDate) // ...using post request
    //         .map(res => res.json()) // ...and calling .json() on the response to return data
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    // }
    getdateslab() {

        return this.http.get(this.slabDate) // ...using post request
            .map(res => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => this.checkError(error)); //...errors if any
    }

   
    reset() {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    }
    newsdetails(){
        this.setHeader();
        return this.http.get(this.newsDetails, { headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }
    checkError(error){
        if(error.status === 401){
            localStorage.removeItem('currentUserAlphaX');
            this.router.navigate(['/login']);
        }
        else{
            return Observable.throw(error.json().error || 'Server error'); 
        }
        return Observable.throw(error.json().error || 'Server error');
    }
    getCount(): Observable<any> {
        this.setHeader();
        return this.http.get(this.getUsercountURL, {
            headers: this.headers
        }).map(res => res.json())
            .catch((error: any) => this.checkError(error));

    }
    checkAndGenerateAlphaxAddress(post): Observable<any>{
        this.setHeader();
        return this.http.post(this.generateAlphaxAddressURL,JSON.stringify(post) ,{ headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }

    checkAndGenerateBTCAddress(post): Observable<any>{
        this.setHeader();
        return this.http.post(this.generateBTCAddressURL,JSON.stringify(post) ,{ headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }
    checkAndGenerateLTCAddress(post): Observable<any>{
        this.setHeader();
        return this.http.post(this.generateLTCAddressURL,JSON.stringify(post) ,{ headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }
    checkAndGenerateBCHAddress(post): Observable<any>{
        this.setHeader();
        return this.http.post(this.generateBCHAddressURL,JSON.stringify(post) ,{ headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }
    checkAndGenerateETHAddress(post): Observable<any>{
        this.setHeader();
        return this.http.post(this.generateETHAddressURL,JSON.stringify(post) ,{ headers: this.headers})
        .map(res => res.json()) // ...and calling .json() on the response to return data
        .catch((error: any) => this.checkError(error)); //...errors if any
    }
}

