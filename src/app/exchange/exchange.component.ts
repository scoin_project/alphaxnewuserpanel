import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { TranslateService } from '@ngx-translate/core';
import { PostsService} from '../posts.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.css']
})
export class ExchangeComponent implements OnInit {
  language:any;

  constructor(public post:PostsService, public app: AppComponent,private translate: TranslateService) {
    
    
    this.language = this.post.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','FAQ_chineese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','FAQ_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','FAQ_italian');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','FAQ_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','FAQ_spanish');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','FAQ_german');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','FAQ_english');

    }
    else {
      this.switchLanguage('en','FAQ_english');

    }

    translate.setDefaultLang('FAQ_english');

   }
   switchLanguage(language: string, address:string) {
    this.post.setLanguage(language);
    this.translate.use(address);
   }

  ngOnInit() {
    this.app.isAutherizes  = false;
  }

}
