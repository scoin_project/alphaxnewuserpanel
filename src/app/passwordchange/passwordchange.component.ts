import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { PostsService } from '../posts.service';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";


@Component({
  selector: 'app-passwordchange',
  templateUrl: './passwordchange.component.html',
  styleUrls: ['./passwordchange.component.css']
})
export class PasswordchangeComponent implements OnInit {
  pass: any;
  rePass: any;
  constructor(public app: AppComponent,private router: Router,
    private postsService: PostsService) { }

  ngOnInit() {
    this.app.isAutherizes  = false;
  }
  resetPass() {
    if (this.pass === this.rePass) {
        var userEmail = localStorage.getItem('email');
        let post = {
            "email": userEmail,
            "password": this.pass
        }
        this.postsService.resetUserPassword(post)
            .subscribe(
            data => {
                if (data) {
                    if (data.status === "OK") {
                         alert(data.msg);
                        // this.dialogService.addDialog(
                        //     AlertModalComponent,
                        //     {
                        //         message: data.msg
                        //     })

                        this.router.navigate(['/login'])
                    }
                    else if (data.status === "ERROR") {
                         alert("Please enter valid details." + data.msg);
                        // this.dialogService.addDialog(
                        //     AlertModalComponent,
                        //     {
                        //         message: 'Please enter valid details.' + data.msg
                        //     })
                        // this._alertService.error('Please enter valid details. ' +  data.msg);
                    }
                } else {
                     alert("Error occurred please try again. " + data.msg);
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: 'Error occurred please try again.' + data.msg
                    //     })
                     //this.error = 'Error occurred please try again.'+ data.msg;
                }
            });
    }
    else {
         alert("Password did not match. Please enter both password correctly")
        // this.dialogService.addDialog(
        //     AlertModalComponent,
        //     {
        //         message: 'Password did not match. Please enter both password correctly'
        //     })
    }
}

}
