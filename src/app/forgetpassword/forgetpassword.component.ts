import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
declare var $:any;

export class forgotData {

  email: string;
}
@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
  loading = false;
  returnUrl: string;
  error: string;

  model: forgotData = {

    email: ""
}
  constructor(private router: Router,
    private postsService: PostsService,) {
      
      if(document.getElementById('mobile-nav-toggle')!= undefined){
        var link = document.getElementById('mobile-nav-toggle');
        link.style.display = 'none';
        $("nav").hide();
    }
     }

  ngOnInit() {
  }
  forgotPass() {
    this.postsService.forgotPassword(this.model)
        .subscribe(
        data => {
            if (data) {
                if (data.status === "OK") {
                    localStorage.setItem('email', this.model.email);
                    this.loading = false;
                    //LoginCustom.displayVerifycodeForm();
                     alert(data.msg);
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: data.msg
                    //     })
                    this.router.navigate(['/securitycode'])
                }
                else if (data.status === "ERROR") {
                     alert("Please enter valid details. " + data.msg);
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: 'Please enter valid details.' + data.msg
                    //     })
                    // this._alertService.error('Please enter valid details. ' +  data.msg);
                }
            } else {
                 alert("Error occurred please try again. " + data.msg);
                // this.dialogService.addDialog(
                //     AlertModalComponent,
                //     {
                //         message: 'Error occurred please try again.' + data.msg
                //     })
                // this.error = 'Error occurred please try again.'+ data.msg;
            }
        
        });
}
}
