import { Component, OnInit,  ViewChild,ElementRef } from '@angular/core';
import { PostsService } from '../posts.service';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { AppComponent } from '../app.component';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

declare var $:any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public aFormGroup: FormGroup;

    logusername: any;
    logpassword: any;
    actToken: any;
    loading = false;
    returnUrl: string;
    error: string;
    captcha:any="";
    sitekey:string="6LdP_XMUAAAAAGe0RqcDIQ1shbIZ8Mln0jwhtyoZ";
    size:any = 'Normal';
    lang:any = 'en';
    theme:any = 'Light';
    type:any = 'Image';
    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        // private http: HttpClient,
        private http: Http,
        private postsService: PostsService,
        public app: AppComponent,
        private formBuilder: FormBuilder
    ) {
        this.actToken = localStorage.getItem("activateAlpaxAccount");
        if (this.actToken != undefined || this.actToken != '') {
            this.activateCodeAccount();
        }


        if(document.getElementById('mobile-nav-toggle')!= undefined){
            var link = document.getElementById('mobile-nav-toggle');
            link.style.display = 'none';
            $("nav").hide();
        }
        
        // if(document.getElementById('mobile-nav')!= undefined){
        //     var link = document.getElementById('mobile-nav');
        //     link.style.display = 'none';
        // }


        
    }

    ngOnInit() {
        localStorage.removeItem('currentUserAlphaX');

        this.app.isAutherizes = false;
        this.aFormGroup = this.formBuilder.group({
            recaptcha: ['', Validators.required]
          });

    }
    activateCodeAccount() {


        let actdata = {
            "actToken": this.actToken
        }
        this.postsService.activateCode(actdata)
            .subscribe(result => {

                if (result.status === "OK") {

                    alert(result.msg);
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: result.msg
                    //     })

                    this.loading = false;
                    this._router.navigate(['/login']);
                    localStorage.removeItem('activateAlpaxAccount');
                    return false;
                } else {
                    alert(result.msg);
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: result.msg
                    //     })
                    this.error = 'Account Not activated';
                    localStorage.removeItem('activateAlpaxAccount');
                    this.loading = false;
                }
            });
    }


    signin() {
        this.loading = false;

        // if(this.logusername.indexOf(' ') >= 0)
        // {
        //     alert("don't enter space");
        // }
        let logindata = {
            "logusername": this.logusername,
            "logpassword": this.logpassword,
            "captcha":this.captcha
        }
        if (this.logusername === undefined || this.logusername === "" || this.logpassword === undefined || this.logpassword === "") {
            alert('Please fill valid login details.');
        } else {
            this.postsService.Login(logindata)
                .subscribe(
                    data => {
                        if(data == false){
                            grecaptcha.reset() 
                        }
                        if (data != undefined) {
                            let twoFaStatus = localStorage.getItem('twoFAstatus');
                            let citizenshipStatusData = JSON.parse(localStorage.getItem('citizenshipStatus'));
                            let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
                            //this._router.navigate(['/index'])
                            if (currentUser != undefined || currentUser != null) {

                                if (currentUser) {

                                    this.postsService.updateBTCtran(currentUser)
                                        .subscribe(
                                            data => {

                                                this._router.navigate([this.returnUrl]);

                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateETHtran(currentUser)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateLTCtran(currentUser)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateBCHtran(currentUser)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this._router.navigate(['/index'])


                                }
                                else if (twoFaStatus) {
                                    let twoFAstatus = JSON.parse(localStorage.getItem('twoFAstatus'));

                                    this.postsService.updateBTCtran(twoFAstatus)
                                        .subscribe(
                                            data => {

                                                this._router.navigate([this.returnUrl]);

                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateETHtran(twoFAstatus)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateLTCtran(currentUser)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });
                                    this.postsService.updateBCHtran(currentUser)
                                        .subscribe(
                                            data => {
                                                //    this._router.navigate(['/index'])
                                                this.loading = false;
                                            },
                                            error => {
                                                // this.showAlert('alertSignin');
                                                // this._alertService.error(error);
                                                this.loading = false;
                                            });

                                }
                            }
                            else {
                                if (twoFaStatus) {
                                    this._router.navigate(['/verify'])
                                }
                                
                            }
                        }
                    },
                    error => {
                        // this.showAlert('alertSignin');
                        // this._alertService.error(error);
                        this.loading = false;
                    });
        }
    }
    resolved(captchaResponse: string) {
    }
    handleSuccess(event){
        this.captcha = event;

    }
    handleExpire(){}
    handleLoad(){}
}
