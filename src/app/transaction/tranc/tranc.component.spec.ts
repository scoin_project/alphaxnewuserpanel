import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrancComponent } from './tranc.component';

describe('TrancComponent', () => {
  let component: TrancComponent;
  let fixture: ComponentFixture<TrancComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrancComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
