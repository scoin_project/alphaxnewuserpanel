import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { FormControl } from '@angular/forms';
import { AppComponent } from '../app.component';

declare var $:any;
//import * as jsPDF from 'jspdf';
//import * as jpt from 'jspdf-autotable';
//import { DatePipe } from '@angular/common';
@Component({
    selector: 'app-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {




    searchBox: FormControl = new FormControl();
    searchBox1: FormControl = new FormControl();
    searchBox2: FormControl = new FormControl();
    searchBox3: FormControl = new FormControl();
    searchBox4: FormControl = new FormControl();

    constructor(private router: Router, private postsService: PostsService, public app: AppComponent, ) {

        if(document.getElementById('mobile-nav-toggle')!= undefined){
            var link = document.getElementById('mobile-nav-toggle');
            link.style.display = 'none';
            // $("nav").hide();
        }
       
        // this.postsService.startService();

        this.searchBox.valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.searchBtcTerm = term;
                this.searchBtc();
            })
        // this.alluser();

        this.searchBox1.valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.searchEthTerm = term;
                this.searchEth();
            })
        this.searchBox2.valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.searchLtcTerm = term;
                this.searchLtc();
            })
            this.searchBox3.valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.searchBchTerm = term;
                this.searchBch();
            })
            this.searchBox4.valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe((term) => {
                this.searchAxTerm = term;
                this.searchAx();
            })


            
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        console.log('currentUser user is ' + JSON.stringify(this.currentUser));
         this.getbtcTransaction();
         this.getethTransaction();
        this.getLocalTransaction();
        this.getltcTransaction();
        this.getbchTransaction();
        this.getwithdrawTransaction();
        this.app.isAutherizes = true;
    }
   


    msg: any;
    error: any;
    p: any;
    p1: any;
    p2: any;
    p3: any;
    p4: any; 
    p5: any;
    currentUser: any;
    btcdata: any = [];
    ethdata: any = [];
    searchBtcTerm: any = "";
    searchEthTerm: any = "";
    searchLtcTerm: any = "";
    searchBchTerm: any = "";
    searchAxTerm: any = "";
    totalbtc: any = [];
    totaleth: any = [];
    totalbch:any=[];
    totalLtc:any=[];
    totalax:any=[];
    totalwithdraw:any=[];
    btcTransactionList: any;
    ethTransactionList: any;
    localTransactionList: any;
    ltcTransactionList: any;
    bchTransactionList: any;
    withdrawTransactionList:any;
    
    getLocalTransaction() {
        let name = {
            "userName": this.currentUser
        }
        this.postsService.getLocalTransaction(name)
            .then(result => {
                if (result) {
                    let transactionLen = (result.data.length);
                    if (transactionLen == 0) {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.localTransactionList = result.data;
                        this.totalax = result.data;
                        this.localTransactionList = this.totalax.map(o => {
                            if(o.trans_type == 'BOUNTY'){
                                return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'BTCbalance': o.BTCbalance, 'transaction': o.transaction, 'totalLocalCoins':(o.bonusCoins).toFixed(8) ,'mainCoinValue': o.mainCoinValue,'transaction_id':o.transaction_id,'trans_type':o.trans_type };
                            }
                            else{
                                return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'BTCbalance': o.BTCbalance, 'transaction': o.transaction, 'totalLocalCoins':(o.totalLocalCoins).toFixed(8) ,'mainCoinValue': o.mainCoinValue,'transaction_id':o.transaction_id,'trans_type':o.trans_type };
                            }
                            
                        })
                       
                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });

    }
    getbtcTransaction() {
        let name = {
            "userName": this.currentUser
        }
        //let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // console.log(JSON.stringify('currentUser user is ' +currentUser));
        console.log("====" + this.currentUser);
        this.postsService.getBTCTransaction(name)
            .then(result => {
                // console.log("btc transaction is");
                //console.log(JSON.stringify(result));
                if (result) {
                    let transactionLen = JSON.stringify(result.data.length);
                    if (transactionLen == "0") {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.btcTransactionList = result.data;
                        this.totalbtc = result.data;
                        this.btcTransactionList = this.totalbtc.map(o => {
                            if(o.transaction != undefined || o.transaction != null){
                            return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'BTCbalance': o.BTCbalance, 'transaction': o.transaction, 'value': o.value,'trans_index':o.transaction.hash,'trans_type':o.trans_type };
                            }
                            else
                            return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'BTCbalance': o.BTCbalance, 'transaction': o.transaction, 'value': o.value,'trans_index':o.trans_index,'trans_type':o.trans_type };
                        })
                        this.totalbtc = this.btcTransactionList;

                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });
    }
    getethTransaction() {
        // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let name = {
            "userName": this.currentUser
        }
        this.postsService.getETHTransaction(name)
            .then(result => {
                // console.log("eth transaction");
                // console.log(JSON.stringify(result));
                if (result) {
                    let transactionLen = JSON.stringify(result.data.length);
                    if (transactionLen == "0") {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.ethTransactionList = result.data;
                        this.totaleth = result.data;
                        this.ethTransactionList = this.totaleth.map(o => {
                            return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'ETHbalance': o.ETHbalance, 'trans_hash': o.trans_hash, 'transaction': o.transaction, 'ethValue': o.ethValue};
                        })

                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });
    }

    getltcTransaction() {
        // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let name = {
            "userName": this.currentUser
        }
        this.postsService.getLTCTransaction(name)
            .then(result => {
                // console.log("eth transaction");
                // console.log(JSON.stringify(result));
                if (result) {
                    let transactionLen = JSON.stringify(result.data.length);
                    if (transactionLen == "0") {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.ltcTransactionList = result.data;
                        this.totalLtc = result.data;
                        this.ltcTransactionList = this.totalLtc.map(o => {
                            return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'value': o.value, 'tx_hash': o.tx_hash, 'transaction': o.transaction };
                        })

                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });
    }


    // getwithdrawtransaction() {
    //     let name = {
    //         "userName": this.currentUser
    //     }
    //     this.postsService.getwithdrawdetails(name)
    //         .subscribe(result => {
                
    //             if (result) {
    //                 let transactionLen = JSON.stringify(result.data.length);
    //                 if (transactionLen == "0") {
    //                     this.msg = "No Any Transaction"
    //                 } else {
    //                     let tran = JSON.stringify(result.data);
    //                     this.withdrwalTransactionList = result.data;
    //                     this.totalwithdrawal = result.data;
    //                     this.withdrwalTransactionList = this.totalwithdrawal.map(o => {
    //                         return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'LTCbalance': o.LTCbalance, 'trans_hash': o.trans_hash, 'transaction': o.transaction };
    //                     })

    //                 }

    //             } else {
    //                 this.error = 'Transaction Data Not get';
    //             }
    //         });
    // }


    getbchTransaction() {
        // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let name = {
            "userName": this.currentUser
        }
        this.postsService.getBCHTransaction(name)
            .then(result => {
                // console.log("eth transaction");
                // console.log(JSON.stringify(result));
                if (result) {
                    let transactionLen = JSON.stringify(result.data.length);
                    if (transactionLen == "0") {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.bchTransactionList = result.data;
                        this.totalbch = result.data;
                        this.bchTransactionList = this.totalbch.map(o => {
                            return { '_id': o._id, 'createdAt': o.createdAt, 'email': o.email, 'fullname': o.fullname, 'username': o.username, 'value': o.value, 'trans_index': o.trans_index, 'transaction': o.transaction };
                        })

                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });
    }
    getwithdrawTransaction() {
        // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let name = {
            "userName": this.currentUser
        }
        this.postsService.withdrawdetails(name)
            .subscribe(result => {
                // console.log("eth transaction");
                // console.log(JSON.stringify(result));
                if (result) {
                    let transactionLen = JSON.stringify(result.data.length);
                    if (transactionLen == "0") {
                        this.msg = "No Any Transaction"
                    } else {
                        let tran = JSON.stringify(result.data);
                        this.withdrawTransactionList = result.data;
                        this.totalwithdraw = result.data;
                        this.withdrawTransactionList = this.totalwithdraw.map(o => {
                            return { '_id': o._id, 'createdAt': o.createdAt,  'amount':o.amount, 'isdecline': o.isdecline,'type':o.type,'isapprove':o.isapprove };
                        })

                    }

                } else {
                    this.error = 'Transaction Data Not get';
                }
            });
    }

    searchBtc() {
        let term = this.searchBtcTerm;
        term = term.toLocaleLowerCase();
        this.btcTransactionList = this.totalbtc.filter(function (res) {
            return res.trans_index.indexOf(term) >= 0;
        });
    }
    searchEth() {
        let term = this.searchEthTerm;
        term = term.toLocaleLowerCase();
        this.ethTransactionList = this.totaleth.filter(function (res) {
            return res.trans_hash.indexOf(term) >= 0;
        });
    }
    searchLtc() {
        let term = this.searchLtcTerm;
        term = term.toLocaleLowerCase();
        this.ltcTransactionList = this.totalLtc.filter(function (res) {
            return res.tx_hash.indexOf(term) >= 0;
        });
    }
    searchBch() {
        let term = this.searchBchTerm;
        term = term.toLocaleLowerCase();
        this.bchTransactionList = this.totalbch.filter(function (res) {
            return res.trans_index.indexOf(term) >= 0;
        });
    }
    searchAx() {
        let term = this.searchAxTerm;
        term = term.toLocaleLowerCase();
        this.localTransactionList = this.totalax.filter(function (res) {
            return res.transaction_id.indexOf(term) >= 0;
        });
    }

    getBlockchainUrl(value) {
        //return "https://blockchain.info/tx/"+value;
        window.open("https://blockchain.info/tx/" + value);
    }
    getEtherscanUrl(value) {
        window.open("https://etherscan.io/tx/" + value);
    }
    getbchUrl(value) {
        window.open("https://explorer.bitcoin.com/bch/tx/" + value);
    }
    getLTCscanUrl(value) {
        window.open("https://live.blockcypher.com/ltc/tx/" + value);
    }
    getAlphascanUrl(value) {
        window.open("https://explorer.alpha-x.io/tx/" + value);
    }


    
}
