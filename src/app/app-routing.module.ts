import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/login/login.component'
import { SignupComponent } from '../app/signup/signup.component'
import { DashboardComponent } from '../app/dashboard/dashboard.component'
import { FaqComponent } from '../app/faq/faq.component';
import { ProfileComponent } from '../app/profile/profile.component';
import { TransactionComponent } from '../app/transaction/transaction.component';
import { PasswordchangeComponent } from '../app/passwordchange/passwordchange.component';
import { ForgetpasswordComponent } from '../app/forgetpassword/forgetpassword.component';
import { TwofaComponent } from '../app/twofa/twofa.component';
import { SecuritycodeComponent } from '../app/securitycode/securitycode.component';
import { AffiliateComponent } from '../app/affiliate/affiliate.component';
import { AuthGuard } from './guard.service';
import {HomeComponent} from './home/home.component';
import {AboutusComponent} from './aboutus/aboutus.component';
import {TeamComponent} from './team/team.component';
import {WebfaqComponent} from './webfaq/webfaq.component';
import {ContactusComponent} from './contactus/contactus.component';
import {ExchangeComponent} from './exchange/exchange.component';
import {PaymentComponent} from './payment/payment.component';
import { LegalInfoComponent } from './legal-info/legal-info.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { InteractualComponent } from './interactual/interactual.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { DisclaimerWhitepaperComponent } from './disclaimer-whitepaper/disclaimer-whitepaper.component';
import { CoinSaleComponent } from './coin-sale/coin-sale.component';
import { NewsComponent } from './news/news.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',

    },
    {
        path: 'index', component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'faq', component: FaqComponent
        ,
        canActivate: [AuthGuard]
    },
    {
        path: 'profile', component: ProfileComponent
        ,
        canActivate: [AuthGuard]
    },
    {
        path: 'transactions', component: TransactionComponent
        ,
        canActivate: [AuthGuard]
    },
    {
        path: 'news', component: NewsComponent,
        canActivate: [AuthGuard]
    },
    { path: 'login', component: LoginComponent },
    {
        path: 'signup', component: SignupComponent
        
    },
    {
        path: 'verify', component: TwofaComponent
        
    },
    {
        path: 'forgotpassword', component: ForgetpasswordComponent,
        
    },
    {
        path: 'securitycode', component: SecuritycodeComponent,
        
    },
    {
        path: 'changepassword', component: PasswordchangeComponent
        
    },
    {
        path: 'affiliate', component: AffiliateComponent,
        canActivate: [AuthGuard]
    },
    // {
    //     path: 'home', component: HomeComponent
    // },
    { 
      path:'aboutus', component: AboutusComponent
   },
   {
    path:'team', component: TeamComponent
   },
   {
     path:'webfaq', component: WebfaqComponent
   },
   {
       path:'contactus', component: ContactusComponent
   },
   {
       path: 'exchange', component: ExchangeComponent
   },
   {
    path: 'legal', component: LegalInfoComponent
},
{
    path: 'privacypolicy', component: PrivacyPolicyComponent
},
{
    path: 'intellectual', component: InteractualComponent
},
{
    path: 'termsconditions', component: TermsConditionsComponent
},
{
    path: 'disclaimer', component: DisclaimerComponent
},
{
    path: 'disclaimerWhitepaper', component: DisclaimerWhitepaperComponent
},
{
    path: 'coinsale', component: CoinSaleComponent
},
   {
    path: 'payment', component: PaymentComponent
}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }