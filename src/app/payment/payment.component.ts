import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import {TranslateService} from '@ngx-translate/core';
import {PostsService} from '../posts.service';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  language:any;
  constructor(public app: AppComponent,private translate: TranslateService,public post:PostsService) { 
    
    this.language = this.post.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','payment_chinese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','PAYMENT_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','italian_payment');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','PAYMENT_french');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','spanish_payment');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','german__payment');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','payment_english');

    }
    else {
      this.switchLanguage('en','payment_english');

    }

    translate.setDefaultLang('payment_english');

   }
   switchLanguage(language: string, address:string) {
     
    this.post.setLanguage(language);
    this.translate.use(address);
  }
 
  ngOnInit() {
    this.app.isAutherizes  = false;
  }

}
