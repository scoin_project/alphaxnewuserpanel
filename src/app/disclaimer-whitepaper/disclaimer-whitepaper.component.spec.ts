import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisclaimerWhitepaperComponent } from './disclaimer-whitepaper.component';

describe('DisclaimerWhitepaperComponent', () => {
  let component: DisclaimerWhitepaperComponent;
  let fixture: ComponentFixture<DisclaimerWhitepaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisclaimerWhitepaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisclaimerWhitepaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
