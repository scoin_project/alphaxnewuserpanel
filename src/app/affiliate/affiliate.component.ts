import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostsService } from '../posts.service';
import { AppComponent } from '../app.component';
declare var $:any;


@Component({
    selector: 'app-affiliate',
    templateUrl: './affiliate.component.html',
    styleUrls: ['./affiliate.component.css']
})
export class AffiliateComponent implements OnInit {
    username: any;
    userName: any;
    constructor(public app: AppComponent, private postsService: PostsService,
        private router: Router) {
            
            if(document.getElementById('mobile-nav-toggle')!= undefined){
                var link = document.getElementById('mobile-nav-toggle');
                link.style.display = 'none';
                // $("nav").hide();
            }
        

    }
    error: string;
    dataJSONArray: any = [];
    datasatus: boolean;
    datasatus1: boolean;
    affiliatelink: any = {};
    subUser: any = "-";
    affiliatelist: Array<any> = [];
    level: number = 1;
    walletAddress:any;
    walletAddressPayout:any;
    btcAddress: any;
    btcAmount: any = 0;
    ethAddress: any;
    ethAmount: any = 0;
    bchAddress: any;
    bchAmount: any = 0;
    ltcAddress: any;
    ltcAmount: any = 0;
    liveUrl: any;
    currentName: any;
    totalBTCequ:any=0;
    totalEQUusd:any=0;
    ltcusd:any=0;
    btcusd:any=0;
    bchusd:any=0;
    ethusd:any=0;
    isValidUSD:boolean=false;
 

    ngOnInit() {
        this.app.isAutherizes = true;
        
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        this.currentName = currentUser.userName;
        this.liveUrl = this.postsService.localURL;
        this.affiliatelink = this.postsService.localURL + "#/signup?reff=" + currentUser.userName;

        this.Affiliate();
        this.getUserDetails();
        this.totalUsdRefer();
        this. getpayout();

    }

    

    getUserDetails() {
        this.postsService.getUserProfile()
            .then(result => {
                if (result) {
                    this.btcAmount = result.data.refBTCBalance;
                    this.ethAmount = result.data.refETHBalance;
                    this.bchAmount = result.data.refBCHBalance;
                    this.ltcAmount = result.data.refLTCBalance;

                } else {
                    this.error = 'Profile Data Not get';
                }
            });
    }

    callReferal(username) {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        let post = {
            "userName": currentUser.userName,
            "fromUser": username
        }
        this.postsService.getReferals(post)
            .subscribe(result => {
                if (result) {
                    if (result.data == false) {
                        alert("No referal found for this user.");
                    }
                    else {
                        let affiliatelistlen = result.data.levelone.length;
                        if (affiliatelistlen != 0) {
                            this.affiliatelist = result.data.levelone;
                            this.subUser = this.affiliatelist[0].username;
                            this.level++;
                            this.dataJSONArray = this.affiliatelist;
                        }
                    }
                } else {
                    this.error = 'Profile Data Not get';
                }
            });
    }
    Affiliate() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        let post = {
            "userName": currentUser.userName,
            "fromUser": currentUser.userName
        }
        this.postsService.getReferals(post)
            .subscribe(result => {
                if (result) {
                    let affiliatelistlen = result.data.levelone.length;

                    // this.btcAmount = result.data.btcBalnew;
                    // this.bchAmount = result.data.bchBalnew;
                    // this.ethAmount = result.data.ethBalnew;
                    // this.ltcAmount = result.data.ltcBalnew;

                    this.affiliatelist = result.data.levelone;
                    this.level = 1;
                    this.subUser = '-';
                    if (affiliatelistlen == "0") {
                        this.datasatus = false;
                        this.datasatus1 = true;
                    } else {
                        this.datasatus = true;
                        this.datasatus1 = false;
                    }
                    this.dataJSONArray = this.affiliatelist;
                } else {
                    this.error = 'Profile Data Not get';
                }
            });

    }
    withdrawRequest(type, amount, address) {

        if (address == "" || address == undefined) {
            alert("Please enter valid address.");
        }
        else if (amount <= 0 || amount == undefined || amount == "") {
            alert("You don't have enough balance.");
        }
        else {
            let post = {
                userName: this.currentName,
                type: type,
                amount: amount,
                address: address
            }
            this.postsService.withdrawRequestApi(post)
                .subscribe(res => {
                    alert(res.msg);
                })
        }
    }
    totalUsdRefer(){
        let userdata = {
            "userName":this.currentName

        }
        this.postsService.getReferalUSD(userdata)
        .subscribe(result => {
            if(result){
                this.totalBTCequ = result.totalbtcequtotalusd;
                this.totalEQUusd = result.totalusd;
                this.ltcusd = result.userltctousd;
                this.btcusd = result.userbtctousd;
                this.bchusd = result.userbchtousd;
                this.ethusd = result.userethtousd;
                this.isValidUSD = result.isusdvalid;
            }

        })
    }

    WithdrawPayoutRequest(){
        let userdata = {
            "userName":this.currentName,
            "amount":this.totalBTCequ,
            "totalusd":this.totalEQUusd,
            "userbtctousd":this.btcusd,
            "userethtousd":this.ethusd,
            "userltctousd":this.ltcusd,
            "userbchtousd":this.bchusd

        }
        this.postsService.getRequestPayout(userdata)
        .subscribe(result => {
            if(result.status == "OK"){
                alert(result.msg);
                this.totalUsdRefer();
                this. getUserDetails();
                $('#withdrawBTC').modal('hide');
                
            }
            else if (result.status == "ERROR"){
                alert(result.msg);

            }
            else{
                alert("Please enter all valid details.");
            }

        })
    }

    getpayout(){
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
      
        let userdata = {
          "userName":currentUser.userName,
        }
        this.postsService.getBTCPayoutAddress(userdata)
        .subscribe(result =>{
      if(result.status = "OK"){
          this.walletAddressPayout = result.data;
      }
        })
      }

    arertcopy() {
        alert("Copied link Successfully!!!");
    }
    getUrl() {
        return (this.liveUrl + "/#/registration?reff=" + this.currentName);
    }
    onTwitterClick() {
        let url = this.liveUrl + "/" + "%23" + "/registration?reff=" + this.currentName;
        // window.open('http://twitter.com/share?url=' + encodeURIComponent(url) + '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
        window.open('http://twitter.com/share?url=' + url);
    }
    onfacebookClick() {
        // https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Falpha-x.io%2F&layout=button&size=small&mobile_iframe=false&width=59&height=20&appId"
        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-x'
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url) +  '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
    }
    onlinkedinclick() {
        // https://www.linkedin.com/shareArticle?mini=true&url=http://developer.linkedin.com&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn
        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        // let text = 'Signup On Phibits'
        window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(url) + '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    onWhatapp() {
        // this.whatappUrl="http://whatsapp://send?text=https://tokensale.Phibits.io/signup/refer/rkOiA2OWX";//urle shared
        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-X'
        window.open('http://whatsapp://send?text=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    onreddit() {
        // https://reddit.com/submit?url={url}&title={title}
        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-X'
        window.open('https://reddit.com/submit?url=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    onmedia() {
        // let url = 'https://medium.com/@phibits';
        let text = 'Signup On Alpha-X'
        window.open('https://medium.com/@phibits', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    onslack() {
        // let url = 'phibits.slack.com';
        // let text = 'Signup On Phibits'
        window.open('https://Alpha-X.slack.com/', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    onTelegram() {
        // https://t.me/share/url?url={url}&text={title}&to={phone_number}

        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-X'
        window.open('https://t.me/share/url?url=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    vk() {
        // https://t.me/share/url?url={url}&text={title}&to={phone_number}

        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-X'
        window.open(' http://vk.com/share.php?url=' + encodeURIComponent(url) + '&text=' + text, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    instagram() {

        let url = this.liveUrl + "/#/registration?reff=" + this.currentName;
        let text = 'Signup On Alpha-X'
        window.open(' http://vk.com/share.php?url=' + url + '&text=' + text, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }
    
}
