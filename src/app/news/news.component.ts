import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  news:Array<any> = [];

  constructor(private postsService: PostsService,public app: AppComponent) { }

  ngOnInit() {
    this.app.isAutherizes = true;
    this.getNewsDetails();
  }
  getNewsDetails(){
    this.postsService.newsdetails()
    .subscribe(res=>{
        if(res.status == 200){
        this.news = res.data;
        }
    })
}

}
