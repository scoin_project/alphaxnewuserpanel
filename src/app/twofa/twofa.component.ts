import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-twofa',
  templateUrl: './twofa.component.html',
  styleUrls: ['./twofa.component.css']
})
export class TwofaComponent implements OnInit {
  error: string;
  returnUrl: string;
  tokendata: any;
  reff: any = {};
  url: any = {};
  twofatoken:any;

  constructor( public app: AppComponent,private postsService: PostsService,
    private router: Router, private _route: ActivatedRoute) { }

  ngOnInit() {
    this.app.isAutherizes  = false;

  }
  loginredirect() {

    localStorage.removeItem('twoFAstatus');
    this.router.navigate(['/login']);
    return false;

}
validateNumber(){
   if(!isNaN(this.twofatoken)) {
return this.twofatoken
   }else{
    this.twofatoken = "";
    return this.twofatoken
   }
}


verification() {
    this.twofatoken = this.validateNumber();

    if(this.twofatoken==""){
    alert("Enter valid number");

}else{

    let twoFAstatus = JSON.parse(localStorage.getItem('twoFAstatus'));
    console.log("token data " + this.twofatoken);
    this.tokendata = {

        twofatoken: this.twofatoken,
        userName: twoFAstatus.userName,

    }

    if (this.twofatoken == "") {

        alert("Please Enter The Valid Details");

    } else {
        this.postsService.verify2fa(this.tokendata)
            .subscribe(
            data => {
                if (data) {
                    if (data.status === "OK") {
                        alert(data.msg);
                        localStorage.removeItem('twoFAstatus');
                        localStorage.setItem('currentUserAlphaX', JSON.stringify(data));
                        this.router.navigate(['/index']);
                        return false;
                    }
                    else if (data.status === "ERROR") {
                        alert(data.msg);
                        // this._alertService.error('Please enter valid details. ' +  data.msg);
                    }
                } else {
                    alert("Error occurred please try again. " + data.msg);
                    // this.error = 'Error occurred please try again.'+ data.msg;
                }

            });
    }
}
}


}
