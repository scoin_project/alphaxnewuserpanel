import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsService } from '../posts.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  backendUrl: any;
  userName:any;
  isImage:boolean = false;
  profileImage:any;
  language:any;
  requestId :any = 'G_VNcFr8s7I'; 
  is_English:boolean = true;
  is_Chinese:boolean = false;
  is_France:boolean = false;
  is_German:boolean = false;
  is_Russian:boolean = false;
  is_Italian:boolean = false;
  is_Spanish:boolean = false;
  constructor(private postsService: PostsService,
    private router: Router, private _route: ActivatedRoute,private translate: TranslateService) {
      this.language = this.postsService.baseLanguage;
      if(this.language === "ch"){
        this.switchLanguage('ch','Back Office _CHINEESE');
  
      }
      else if(this.language === "rus"){
        this.switchLanguage('rus','Back Office_RUSSIAN');
  
      }
      else if(this.language === "ita"){
        this.switchLanguage('ita','Back Office_ITALIAN');
  
      }
      else if(this.language === "fr"){
        this.switchLanguage('fr','Back Office_FRENCH');
  
      }
      else if(this.language === "spa"){
        this.switchLanguage('spa','Back Office_SPANISH');
  
      }
      else if(this.language === "ger"){
        this.switchLanguage('ger','Back Office_GERMAN');
  
      }
      else if(this.language === "en"){
        this.switchLanguage('en','Back Office_ENGLISH');
  
      }
      else {
        this.switchLanguage('en','Back Office_ENGLISH');
  
      }
  
      translate.setDefaultLang('Back Office_ENGLISH');
     }
     switchLanguage(language: string, address:string) {
      var element = document.getElementById("myDIV");
  
      if(element != null){
      if(language === 'en'){
        this.requestId = 'G_VNcFr8s7I';
        element.className = 'flag-icon flag-icon-gb';
      }
      else if(language === 'ch'){
        this.requestId = 'G_VNcFr8s7I';
        element.className = 'flag-icon flag-icon-cn';
      }
      else if(language === 'rus'){
        this.requestId = 'XAb3SYVHj84';
        element.className = 'flag-icon flag-icon-me';
      }
      else if(language === 'ita'){
        this.requestId = 'sPWhDh5U-KE';
        element.className = 'flag-icon flag-icon-it';
      }
      else if(language === 'spa'){
        this.requestId = 'GBZg0a7nxqc';
        element.className = 'flag-icon flag-icon-es';
      }
      else if(language === 'ger'){
        this.requestId = 'XAUQ5pFyvQo';
        element.className = 'flag-icon flag-icon-de';
      }
      else if(language === 'fr'){
        this.requestId = 'SP-56TbXGO0';
        element.className = 'flag-icon flag-icon-fr';
      }
  
    }
    //  element.classList.add("flag-icon flag-icon-cn");
  
      this.postsService.setLanguage(language);
      this.translate.use(address);
  }
  ngOnInit() {
    this.backendUrl = this.postsService.baseUrl;
    this.getUserDetails();
  }
  logout(){
    localStorage.removeItem('currentUserAlphaX');
    this.router.navigate(['/login']);
  }
  getUserDetails() {

    this.postsService.getUserProfile()
      .then(res => {
        if (!res.error) {
          this.userName = (res.data.userName);
          if (res.data.profile_pic == "") {
            this.isImage = false;
            this.profileImage = res.data.profile_pic;
          }
          else {
            this.isImage = true;
            this.profileImage = res.data.profile_pic;
          }
        }
      })
  }

}
