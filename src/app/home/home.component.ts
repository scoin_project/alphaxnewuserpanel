import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { BrowserModule, DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, timer } from 'rxjs';
import { take, map } from 'rxjs/operators';


declare var $ : any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
@Pipe({ name: 'safe' })

export class HomeComponent implements AfterViewInit, OnInit, PipeTransform {
  btclive: any = 0;
  ethlive: any = 0;
  bchlive: any = 0;
  ltclive: any = 0;
  axlive: any=0;
  tokensold: any;
  totaltoken: any;
  tokenremaining: any;
  percentage: any;
  backendUrl: any;
  profileImage: any;
  userName: any="";
  isImage: boolean = false;
  isLoggedIn: boolean = false;
  slab1: any;
  slab2: any = 0;
  slab3: any = 0;
  precentageSlab1: any;
  precentageSlab2: any = 0;
  precentageSlab3: any = 0;
  count:any=0;
  isImage2:boolean = false;
  isImage1:boolean = true;

  language: any;
  is_English: boolean = true;
  is_Chinese: boolean = false;
  is_France: boolean = false;
  is_German: boolean = false;
  is_Russian: boolean = false;
  is_Italian: boolean = false;
  is_Spanish: boolean = false;
  src: SafeResourceUrl = "https://www.youtube.com/embed/G_VNcFr8s7I";

  requestId: any = 'G_VNcFr8s7I';
  reuestVideo:any='F3LuoSzcxwY';


  day: number = 0;
  hrs: number = 0;
  min: number = 0;
  sec: number = 0;

  dayS:string = "0";
  hrsS:string = "0";
  minS:string = "0";
  secS:string = "0";

  isVisible:boolean=false;
  isLoaded : boolean = false;
  items: Array<any> = [];

  constructor(public app: AppComponent, private router: Router, private sanitizer: DomSanitizer, public post: PostsService, private translate: TranslateService) {

    window.scrollTo(0,0);

    let user_Name = JSON.parse(localStorage.getItem('currentUserAlphaX'));
    if (user_Name != undefined || user_Name != null) {
      this.isLoggedIn = true;
      this.backendUrl = this.post.baseUrl;
      this.getUserDetails();
    }
  }
  switchLanguage(language: string, address: string) {
    var element = document.getElementById("myDIV");

    if (element != null) {
      if (language === 'en') {
        this.requestId = 'G_VNcFr8s7I';
        // this.reuestVideo = 'F3LuoSzcxwY'
        element.className = 'flag-icon flag-icon-gb';
      }
      else if (language === 'ch') {
        this.requestId = 'S28aZw_1igU';
        // this.reuestVideo = 'SqaUsslE12M'

        element.className = 'flag-icon flag-icon-cn';
      }
      else if (language === 'rus') {
        this.requestId = 'XAb3SYVHj84';
        // this.reuestVideo = 'GdXZjCMYQg4'

        element.className = 'flag-icon flag-icon-me';
      }
      else if (language === 'ita') {
        this.requestId = 'sPWhDh5U-KE';
        // this.reuestVideo = 'RUwlVb3DCN8'

        element.className = 'flag-icon flag-icon-it';
      }
      else if (language === 'spa') {
        // this.requestId = 'GBZg0a7nxqc';
        this.requestId = 'tOn75Zt1Cqk';
        // this.reuestVideo = 's0DyfgbyWCQ'

        element.className = 'flag-icon flag-icon-es';
      }
      else if (language === 'ger') {
        this.requestId = 'XAUQ5pFyvQo';
        // this.reuestVideo = 'SP9u5JnVRx4'

        element.className = 'flag-icon flag-icon-de';
      }
      else if (language === 'fr') {
        this.requestId = 'SP-56TbXGO0';
        // this.reuestVideo = '_4N46dVx9nE'

        element.className = 'flag-icon flag-icon-fr';
      }
      else if (language === 'fr') {
        this.src = "https://www.youtube.com/embed/SP-56TbXGO0";
        // this.src = "https://www.youtube.com/embed/tOn75Zt1Cqk";
        element.className = 'flag-icon flag-icon-fr';
      }
    }
    //  element.classList.add("flag-icon flag-icon-cn");

    this.post.setLanguage(language);
    this.translate.use(address);
    this.setTimer();
  }

  setTimer() {
    let id = setInterval(()=>{

    let now = new Date();
    let eventDate = new Date("March 15, 2019 23:59:59");
    let currentTime = now.getTime();
    let evenTime = eventDate.getTime();

    let remTime = evenTime - currentTime;

    this.sec = Math.floor(remTime / 1000);
    this.min = Math.floor(this.sec / 60);
    this.hrs = Math.floor(this.min / 60);
    this.day = Math.floor(this.hrs / 24);

    this.hrs %= 24;
    this.min %= 60;
    this.sec %= 60;

    //this.hrs = (this.hrs) : this.hrs;
    //this.min = (this.min < 10) ? +(this.min) : this.min;
    //this.sec = (this.sec < 10) ? +(this.sec) : this.sec;
    
    if(this.hrs<10){
      this.hrsS = ("0" + this.hrs).slice(-2);
    }
    if(this.min<10){
      this.minS = ("0" + this.min).slice(-2);
    }
    if(this.sec<10){
      this.secS = ("0" + this.sec).slice(-2);
    }
    
  },1000)
    
    // $('.seconds').text(sec);
    // $('.minutes').text(min);
    // $('.hours').text(hur);
    // $('.days').text(day);

    // setTimeout(this.setTimer(), 1000);
  }
 
  
  ngOnInit() {
    this.getliveValue();
    this.gettokendetails();
    this.getSlabDetails();
    this.getCount();
    //  this.uploadModal();
    //  $('#exampleModalLong').modal('show');

    this.app.isAutherizes = false;

    let user_Name = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
    if (user_Name != undefined || user_Name != null) {
      this.isLoggedIn = true;
      this.backendUrl = this.post.baseUrl;
      this.getUserDetails();
    }

  }
  ngAfterViewInit() {

    this.language = this.post.baseLanguage;
    if (this.language === "ch") {
      this.switchLanguage('ch', 'HOME_chineese');

    }
    else if (this.language === "rus") {
      this.switchLanguage('rus', 'HOME_russian');

    }
    else if (this.language === "ita") {
      this.switchLanguage('ita', 'HOME_italian');

    }
    else if (this.language === "fr") {
      this.switchLanguage('fr', 'HOME_french');

    }
    else if (this.language === "spa") {
      this.switchLanguage('spa', 'HOME_spanish');

    }
    else if (this.language === "ger") {
      this.switchLanguage('ger', 'HOME_german');

    }
    else if (this.language === "en") {
      this.switchLanguage('en', 'HOME_english');

    }
    else {
      this.switchLanguage('en', 'HOME_english');

    }

    // translate.setDefaultLang('HOME_english');

  }

  transform(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  stopVideo() {
    // var src = $('iframe.'+id).attr('src');
    // $('iframe.'+id).attr('src','');
    // $('iframe.'+id).attr('src',src);
    $("#myModal").on('hidden.bs.modal', function (e) {
      $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
    });
  }
  getliveValue() {
    this.post.getLive()
      .subscribe(result => {
        if (result) {
          this.btclive = result.btcval;
          this.ethlive = result.ethval;
          this.bchlive = result.bchval;
          this.ltclive = result.ltcval;
          this.axlive = result.alphaxval;
        }
      })
  }
  // generateUSD() {
  //   // this.loaderService.addLoaderDialog();
  //   let data = {
  //       "userName": this.userName,

  //   }
  //   this.post.generatealphaxvalue(data)
  //       .subscribe(result => {

  //           if (result) {
  //               this.btctousd = result.userbtctousd;
  //               this.ethtousd = result.userethtousd;
  //               this.bchtousd = result.userbchtousd;
  //               this.ltctousd = result.userltctousd;
  //               this.bchtoalphax = result.bchtoalphax;
  //               this.btctoalpha = result.btctoalpha;
  //               this.ethtoalphax = result.ethtoalphax;
  //               this.ltctoalphax = result.ltctoalphax;
  //               this.slabValue = result.current_slab_value;
  //               this.startdate = result.start_date;

  //               this.enddate = result.end_date;

  //               this.checkLimitedUSD();


  //           }
  //       })

  // }
  gettokendetails() {

    this.post.getsoldtokenDetails()
      .subscribe(result => {
        if (result) {
          this.tokensold = result.data.TokenSold;
          this.totaltoken = result.data.totalToken;
          this.tokenremaining = result.data.TokenRemaining;
          this.percentage = (this.tokensold / this.totaltoken) * 100;
        }
      })
  }


  logout() {
    localStorage.removeItem('currentUserAlphaX');
    this.isLoggedIn = false;
    this.ngOnInit();
    // this.router.navigate(['/login']);
  }
  getUserDetails() {

    this.post.getUserProfile()
      .then(res => {
        if (!res.error) {
          this.isLoaded = true;
          this.userName = (res.data.userName);
          if (res.data.profile_pic == "") {
            this.isImage = false;
            this.profileImage = res.data.profile_pic;
          }
          else {
            this.isImage = true;
            this.profileImage = res.data.profile_pic;
          }
        }
      })
  }

  checkAlphaxAddress(){
    let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
    this.post.checkAndGenerateAlphaxAddress({"username":userName})
    .subscribe(res => {
      if (!res.error) {
        console.log("Try Again");
      }
    })
  }
  checkBTCAddress(){

  }
  checkBCHAddress(){

  }
  checkETHAddress(){

  }
  checkLTCAddress(){

  }

  getSlabDetails() {

    this.post.getdateslab()
      .subscribe(result => {
        if (result) {
          this.slab1 = result.slab1;
          this.precentageSlab1 = result.persent1;

          if (result.persent2 > 0) {
            this.slab2 = result.slab2;
            this.precentageSlab2 = result.persent2;


          }

          if (result.persent3 > 0) {

            this.slab3 = result.slab3;
            this.precentageSlab3 = result.persent3;

          }
        }
      })
  }
  uploadModal(){
    $("#mymodal").modal("hide");
  }
 
  buyVideo() {
    
    // this.language = this.post.baseLanguage;
    if (this.post.baseLanguage === "ch") {
      window.open("https://www.youtube.com/watch?v=" + 'SqaUsslE12M');

    }
    else if (this.post.baseLanguage === "rus") {
      window.open("https://www.youtube.com/watch?v=" + 'GdXZjCMYQg4');

    }
    else if (this.post.baseLanguage === "ita") {
      window.open("https://www.youtube.com/watch?v=" + 'RUwlVb3DCN8');

    }
    else if (this.post.baseLanguage === "fr") {
      window.open("https://www.youtube.com/watch?v=" + '1oYX4G4y8UM');

    }
    else if (this.post.baseLanguage === "spa") {
      window.open("https://www.youtube.com/watch?v=" + 's0DyfgbyWCQ');

    }
    else if (this.post.baseLanguage === "ger") {
      window.open("https://www.youtube.com/watch?v=" + 'SP9u5JnVRx4');

    }
    else if (this.post.baseLanguage === "en") {
      window.open("https://www.youtube.com/watch?v=" + 'F3LuoSzcxwY');

    }
    else {
      window.open("https://www.youtube.com/watch?v=" + 'F3LuoSzcxwY');

    }

}
closeModal(){
  this.isVisible = false;
}
callFunction(){
  this.router.navigate(['/signup']);
  //window.open("https://bitcointalk.org/index.php?topic=5035095.msg46012090#msg46012090");
}
getCount(){
  this.post.getCount()
  .subscribe(res=>{
    if(res.status=="OK"){
      this.count = res.data;
    }
  })
}

imageClick(value){

  console.log("comes"+value);
  if(value == 1){
    this.isImage1 = true;
    this.isImage2 = false;
  }
  else if(value == 2){
    this.isImage1 = false;
    this.isImage2 = true;
  }
}

}
