import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { TranslateService } from '@ngx-translate/core';
import { PostsService} from '../posts.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements AfterViewInit, OnInit {

  language:any;
  requestId :any = 'G_VNcFr8s7I'; 


  constructor(public post:PostsService, public app: AppComponent,private translate: TranslateService) {
    
    
    window.scrollTo(0,0);

    // translate.setDefaultLang('team_english');
    

   }
   switchLanguage(language: string, address:string) {
    var element = document.getElementById("myDIV");

    if(element != null){
    if(language === 'en'){
      this.requestId = 'G_VNcFr8s7I';
      element.className = 'flag-icon flag-icon-gb';
    }
    else if(language === 'ch'){
      this.requestId = 'S28aZw_1igU';
      element.className = 'flag-icon flag-icon-cn';
    }
    else if(language === 'rus'){
      this.requestId = 'XAb3SYVHj84';
      element.className = 'flag-icon flag-icon-me';
    }
    else if(language === 'ita'){
      this.requestId = 'sPWhDh5U-KE';
      element.className = 'flag-icon flag-icon-it';
    }
    else if(language === 'spa'){
      // this.requestId = 'GBZg0a7nxqc';
      this.requestId = 'tOn75Zt1Cqk';
      element.className = 'flag-icon flag-icon-es';
    }
    else if(language === 'ger'){
      this.requestId = 'XAUQ5pFyvQo';
      element.className = 'flag-icon flag-icon-de';
    }
    else if(language === 'fr'){
      this.requestId = 'SP-56TbXGO0';
      element.className = 'flag-icon flag-icon-fr';
    }
    else if(language === 'fr'){
      // this.src = "https://www.youtube.com/embed/tOn75Zt1Cqk";
      element.className = 'flag-icon flag-icon-fr';
    }
  }
  //  element.classList.add("flag-icon flag-icon-cn");

    this.post.setLanguage(language);
    this.translate.use(address);
}
  
  ngOnInit() {
    this.app.isAutherizes  = false;
    
  }
  ngAfterViewInit() {

    this.language = this.post.baseLanguage;
    if(this.language === "ch"){
      this.switchLanguage('ch','Team_chinese');

    }
    else if(this.language === "rus"){
      this.switchLanguage('rus','TEAM_russian');

    }
    else if(this.language === "ita"){
      this.switchLanguage('ita','team_itallian');

    }
    else if(this.language === "fr"){
      this.switchLanguage('fr','french_team');

    }
    else if(this.language === "spa"){
      this.switchLanguage('spa','spanish_team');

    }
    else if(this.language === "ger"){
      this.switchLanguage('ger','german__Team');

    }
    else if(this.language === "en"){
      this.switchLanguage('en','team_english');

    }
    else {
      this.switchLanguage('en','team_english');

    }

  }

}
