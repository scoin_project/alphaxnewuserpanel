import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
declare var $:any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  btcAddress:any
  isLoaded: any = false;
  userName: any;
  fullName: any;
  emailId: any;
  phone: any;
  country: any;
  oldpassword: any = '';
  newpassword: any = '';
  repassword: any = '';
  isEdited: any = false;
  profileImage: any;
  profPicture: File;
  passportPic: File;
  residencePic: any;
  selfiePic: any;
  form: FormGroup;
  isImage: boolean = false;
  backendUrl: any;
  kycvalue: any;
  isValidpassport: boolean = false;
  isValidresidence: boolean = false;
  isValidself: boolean = false;
  isValidImage: boolean = false;
  liveUrl:any;
  BTCPayoutAddress:any = "";
    code2fa: "";
    fa2secret:any = true;
    error: string;
    imgdata: "";
    scrtKey: "";
  twofastatus:string = "OFF";
  copyURL:string;
  isKycdone:boolean = false;
  filename:any = '';
  idProofName:any = '';
  addressProofName:any = '';
  photoProofName:any = ''

  constructor(private postsService: PostsService,
    private fb: FormBuilder, private router: Router, private _route: ActivatedRoute,
    public app: AppComponent) { 
      
      if(document.getElementById('mobile-nav-toggle')!= undefined){
        var link = document.getElementById('mobile-nav-toggle');
        link.style.display = 'none';
        // $("nav").hide();
    }
   


    }

  ngOnInit() {
    this.app.isAutherizes = true;
    this.getUserDetails();
    this.liveUrl = this.postsService.localURL;
    this.backendUrl = this.postsService.baseUrl;
    this.get2FaQrCode();
  this.get2fastatusCheck();
  this.getpayout();
  this.getKycDetails();
  }

  getUserDetails() {

    this.postsService.getUserProfile()
      .then(res => {
        if (!res.error) {
          console.log("=========userdetails");
          console.log(res);
          this.isLoaded = true;
          this.userName = (res.data.userName);
          this.fullName = res.data.fullName;
          this.emailId = res.data.email;
          // this.country = res.data.country;
          // this.phone = res.data.phone;
          if (res.data.profile_pic == "") {
            this.isImage = false;
            this.profileImage = res.data.profile_pic;
          }
          else {
            this.isImage = true;
            this.profileImage = res.data.profile_pic;
          }
        }
      })
    this.form = this.fb.group({
      name: ['', Validators.required],
      avatar: null
    });
  }
  saveProfile() {
    if (this.isfullname(this.fullName)) {
      // if (this.isPhone(this.phone)) {
        if (this.isEmail(this.emailId)) {
          let formData: FormData = new FormData();
          formData.append("username", this.userName);
          formData.append("fullname", this.fullName);
          formData.append("email", this.emailId);
          // formData.append("country", this.country);
          // formData.append("phonenumber", this.phone);
          if (this.profPicture != null || this.profPicture != undefined) {
            formData.append("profile_pic", this.profPicture);
          }

          this.postsService.updateDetails(formData)
            .subscribe(result => {
              let bodyString = JSON.stringify(result);
              if (result.status === 'OK') {
                console.log(result);
                alert(result.msg);
                this.isEdited = false;
              }
            });
        } else {
          alert('Please enter valid Email');
        }
      // } else {
      //   alert('Please enter valid Phone Number');
      // }
    } else {
      alert('Please enter valid Fullname');
    }
  }
  isfullname(inputtxt) {
    var letters = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
    if ((inputtxt.match(letters))) {
        return true;
    }
    else {
        return false;
    }
  }
  isEmail(search: string) {
  
    var serchfind: boolean;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
    let regexp = new RegExp('^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');
    // alert("Please enter valid email")
    serchfind = re.test(search.toLowerCase());
  
    //console.log(serchfind)
    return serchfind
  }
  isPhone(inputtxt) {
    var phoneno = /^\d{10,14}$/;
    if ((inputtxt.match(phoneno))) {
        return true;
    }
    else {
        return false;
    }
  }
  onFileChange(event) {
    var file = event.target.files[0];
    if (!file.type.match('image/.*')) {
        //alert("only images");
        alert('only image files are allowed!');
        this.isValidImage = true;
    }
    else {
        this.filename = file.name;
        this.isValidImage = false;
    }
    if (this.isValidImage == false) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        let value = reader.result.split(',')[1];
        this.profPicture = (file);
      };
    }
  }
  }
  
  
  
  get2FaQrCode() {
    let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
    this.postsService.get2FaQrCodeAddress(currentUser)
        .subscribe(result => {
            if (result) {
                this.imgdata = result.data.imageData;
                this.scrtKey = result.data.secretKey;
            }
            else {
                this.error = 'Data Not found';
            }
        });
  }
  
  get2fastatusCheck() {
    let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
  
    this.postsService.get2fastatusApi(currentUser)
        .subscribe(result => {
  
            if (result.status === "OK") {
                this.fa2secret = result.data.twofaStatus;
                if(result.data.twofaStatus)
                {
                  this.twofastatus = "ON";
                }
                else{
                  this.twofastatus = "OFF";
                }
            }
            else {
                this.fa2secret = result.data.twofaStatus;
                this.error = 'Data Not found';
            }
        });
  }
  
  validateEnable() {
    let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
    this.postsService.validateandEnableApi({
        "userName": currentUser.userName,
        "twofatoken": this.code2fa,
    })
        .subscribe(result => {
            if (result.status == 'OK') {
                alert(result.msg);
                this.get2fastatusCheck();
                this.code2fa = "";
            }
            else {
                alert(result.msg);
            }
        });
  }
  getchangePassword() {
    if (this.oldpassword === '' || this.newpassword === '' ) {
         alert('Please fill all valid details')
    }
    else{
      let changedata = {
        "userName": this.userName,
        "oldpassword": this.oldpassword,
        "newpassword": this.newpassword,
    }
    this.postsService.passwordchange(changedata)
        .subscribe(result => {
            let bodyString = JSON.stringify(result);
            console.log('user' + JSON.stringify(result.data));

            if (result) {
                console.log(result);
                
                 alert(result.msg);
                // this.dialogService.addDialog(
                //     AlertModalComponent,
                //     {
                //         message: result.msg
                //     })
                this.oldpassword = '';
                this.newpassword = '';
                
            }
            else {
                
            }

        });
    }
        

   
}
getpayout(){
  let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));

  let userdata = {
    "userName":currentUser.userName,
  }
  this.postsService.getBTCPayoutAddress(userdata)
  .subscribe(result =>{
    if(result.status = "OK"){
      this.BTCPayoutAddress = result.data;
  }

  })
}
updatepayout(){
  let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));

  if(this.BTCPayoutAddress == "" || this.BTCPayoutAddress == null || this.BTCPayoutAddress == undefined){

    alert("Please enter BTC payout address");
  }
  else{
    let userdata = {
      "userName":currentUser.userName,
      "BTCPayoutAddress":this.BTCPayoutAddress
    }
    this.postsService.updatePayoutBTC(userdata)
    .subscribe(result =>{
      if(result.status == "OK"){
        alert(result.msg);
       }
      else if(result.status == "ERROR"){
        alert(result.msg);
       }
       else{
        alert(result.msg);
       }
      
    })

  }
 
}

onPassportChange(event) {

  var file = event.target.files[0];
  if (!file.type.match('image/.*')) {
      
          alert('only image files are allowed!');
      this.isValidpassport = true;
  }
  else {
      this.idProofName=file.name;
      this.isValidpassport = false;
  }
  if (this.isValidpassport == false) {
      let reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
              let value = reader.result.split(',')[1];
              let data = {
                  "lastModified": file.lastModified,
                  "filename": file.name,
                  "lastModifiedDate": file.lastModifiedDate,
                  "size": file.size,
                  "type": file.type
              }
              console.log(event.target.files[0]);
              let file1: File = event.target.files[0];
              let formData = new FormData();
              formData.append('degree_attachment', file, file.name);
              console.log("Form data");
              console.log(formData);

              console.log(file1);
              this.passportPic = file1;
          };
      }
  }
  // else{
  //     alert("Only jpg/jpeg/gif and png files are allowed!");
  // } 
}
onResidenceChange(event) {
  var file = event.target.files[0];
  if (!file.type.match('image/.*')) {
      
          alert('only image files are allowed!')
      this.isValidresidence = true;
  }
  else {
      this.addressProofName = file.name;
      this.isValidresidence = false;
  }
  if (this.isValidresidence == false) {
      let reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
              let value = reader.result.split(',')[1];
              this.residencePic = (file);
          };
      }
  }
  // else{
  //     alert("Only jpg/jpeg/gif and png files are allowed!");
  // } 
}
onSelfieChange(event) {
  var file = event.target.files[0];
  if (!file.type.match('image/.*')) {
      // alert("only images");
     
          alert('only image files are allowed!');
      this.isValidself = true;
  }
  else {
      this.photoProofName = file.name;
      this.isValidself = false;
  }
  if (this.isValidself == false) {
      let reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
              let value = reader.result.split(',')[1];
              this.selfiePic = (file);
          };
      }
  }
  // else{
  //     alert("Only jpg/jpeg/gif and png files are allowed!");
  // }
}
postKycDetails() {
    this.isKycdone = true;

  if (this.passportPic == null || this.residencePic == null || this.selfiePic == null) {
      if (this.isValidpassport == false && this.isValidresidence == false && this.isValidself == false) {
          // alert('Please fill all details')
                this.isKycdone = false;
              alert('Please fill all details');

      }
      else {
                this.isKycdone = false;
              alert('Only jpg , jpeg , gif and png image file formats are allowed!')
      }
  }

  else {

         
      let formData: FormData = new FormData();
      formData.append("username", this.userName);
      formData.append("passport", this.passportPic);
      formData.append("residence_proof", this.residencePic);
      formData.append("selfie", this.selfiePic);

      this.postsService.submitKyc(formData)
          .then(result => {
              let bodyString = JSON.stringify(result);
              console.log('user data is ' + JSON.stringify(result.data));
              if (result) {
                  console.log('result kyc' + result);
                  
                    alert(result.msg);
                  this.getUserDetails();
                  this.getKycDetails();
                  this.isKycdone = false;
              }
          });
  }
}
getKycDetails() {
  let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
  let name = {
      "username": currentUser.userName,
  }
  this.postsService.kycAlreadyexist(name)
      .subscribe(result => {
          if (result) {
              this.kycvalue = result.data;
          }
          else {
              this.kycvalue = false;
          }

      });
}
arertcopy(){
  alert("Copied link Successfully!!!");
}
  getUrl(){
    return (this.liveUrl + "/#/registration?reff=" + this.userName);
  }
}