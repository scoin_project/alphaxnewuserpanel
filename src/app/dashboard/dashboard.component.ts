import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
// import * as $ from 'jquery';


declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    userName: any;
    error: any;
    btcData: any;
    ethData: any;
    alphaxData: any;
    bchData: any;
    ltcData: any;
    btctousd: any;
    ethtousd: any;
    bchtousd: any;
    ltctousd: any;
    btcBalance: any;
    totaltoken: any;
    tokensold: any = 0;
    tokenremaining: any;
    btcBalanceStatus: boolean = false;
    ethBalance: any;
    ethBalanceStatus: boolean = false;
    bchBalance: any;
    bchBalanceStatus: boolean = false;
    ltcBalance: any;
    ltcBalanceStatus: boolean = false;

    alphaxbalances: any = 0;
    alphaBalanceStatus: boolean = false;
    ethpayoutAddressStatus: boolean = false;
    walletAddress: any;
    kycvalue: boolean;
    btcEquiPHI: any = 0;
    ethEquiPHI: any = 0;;
    isBtcEnabled: boolean = true;
    isEthEnabled: boolean = false;
    isLoaded = false;
    btcVal: any;
    ethVal: any;
    usdETH: any
    usdPhi: any;
    usdBTC: any;
    totalICOalphax: any;
    ICOSold: any;
    ICORemaining: any;
    bchtoalphax: any;
    btctoalpha: any;
    ethtoalphax: any;
    ltctoalphax: any;
    minUSDBTC: any;
    minUSDETH: any;
    minUSDBCH: any;
    minUSDLTC: any;
    slabValue: any;

    btclive: any;
    ethlive: any;
    bchlive: any;
    ltclive: any;
    calbtc: any;
    caleth: any;
    calbch: any;
    calltc: any;
    btcstatus: boolean = false;
    ethstatus: boolean = false;
    ltcstatus: boolean = false;
    bchstatus: boolean = false;
    calUSD: any;
    calvalue: any;
    calAx: any;
    currency: any;
    alphaXlive: any;
    startdate: Date;
    enddate: Date;
    percentage: any = 0;

    minUSDStatus: boolean = false;
    buystatus: boolean = true;

    buystatusBTC: boolean = true;
    buystatusETH: boolean = true;
    buystatusBCH: boolean = true;
    buystatusLTC: boolean = true;

    selectedCurrency: any = "BTC";
    rangeValue: any = 20;
    
    day: number = 0;
    hrs: number = 0;
    min: number = 0;
    sec: number = 0;
    isVisible:boolean=true;
    news:Array<any> = [];
    constructor(private postsService: PostsService,
        private router: Router, private _route: ActivatedRoute, public app: AppComponent,
        private spinner: NgxSpinnerService, private translate: TranslateService, ) {
        translate.setDefaultLang('en');

        if (document.getElementById('mobile-nav-toggle') != undefined) {
            var link = document.getElementById('mobile-nav-toggle');
            link.style.display = 'none';
            // $("nav").hide();
        }

     

        this.setTimer();

       
        
    }
    switchLanguage(language: string) {
        this.translate.use(language);
        
    }
    setTimer() {
        let id = setInterval(()=>{
    
          let now = new Date();
        let eventDate = new Date("March 15, 2019 23:59:59");
        let currentTime = now.getTime();
        let evenTime = eventDate.getTime();
    
        let remTime = evenTime - currentTime;
    
        this.sec = Math.floor(remTime / 1000);
        this.min = Math.floor(this.sec / 60);
        this.hrs = Math.floor(this.min / 60);
        this.day = Math.floor(this.hrs / 24);
    
        this.hrs %= 24;
        this.min %= 60;
        this.sec %= 60;
    
        this.hrs = (this.hrs < 10) ? 0 + this.hrs : this.hrs;
        this.min = (this.min < 10) ? 0 + this.min : this.min;
        this.sec = (this.sec < 10) ? 0 + this.sec : this.sec;
    
        },1000)}
        
    ngOnInit() {
        this.userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.getBalances();
        this.getETH();
        this.getBtc();
        this.getBch();
        this.getltc();
        this.getAlphax();
        this.generateUSD();
        this.gettokendetails();
        this.minimumBalUSD();
        this.getliveValue();
        this.app.isAutherizes = true;
        this.updateAll();
        this.refreshBtc();
        this.refreshEth();
        this.refreshLtc();
        this.refreshBch();
        this.getKycDetails();
        this.getNewsDetails();
        this.checkAlphaxAddress();

        this.checkBTCAddress();
        this.checkLTCAddress();
        this.checkBCHAddress();
        this.checkETHAddress();

    }


    getNewsDetails(){
        this.postsService.newsdetails()
        .subscribe(res=>{
            if(res.status == 200){
            this.news = res.data;
            }
        })
    }

    getBalances() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        this.postsService.getAllBalanceApi(currentUser)
            .subscribe(result => {
                if (result) {
                    this.btcBalance = result.data.BTCbalance;
                    this.ethBalance = result.data.ETHbalance;
                    this.bchBalance = result.data.BCHbalance;
                    this.ltcBalance = result.data.LTCbalance;
                    this.alphaxbalances = result.data.alphaxbalances;
                    if (this.alphaxbalances > 0)
                        this.alphaBalanceStatus = true;
                    if (this.btcBalance > 0)
                        this.btcBalanceStatus = true;
                    if (this.ethBalance > 0)
                        this.ethBalanceStatus = true;
                    if (this.bchBalance > 0)
                        this.bchBalanceStatus = true;
                    if (this.ltcBalance > 0)
                        this.ltcBalanceStatus = true;

                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }
    getAlphax() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        this.postsService.getalphaXAddress(currentUser)
            .subscribe(result => {
                if (result) {
                    this.alphaxData = result.data;
                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }
    getETH() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        this.postsService.ethAddress(currentUser)
            .subscribe(result => {
                if (result) {
                    // this.imgdata = result.data.imageData;
                    //this.scrtKey = result.data.secretKey;
                    this.ethData = result.data;
                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }
    getBch() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        let data = {
            "userName": currentUser
        }
        this.postsService.bchAddress(data)
            .subscribe(result => {
                if (result) {
                    this.bchData = result.data;
                    ///this.imgdata = result.data.imageData;
                    // this.scrtKey = result.data.secretKey;
                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }
    getltc() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        let data = {
            "userName": currentUser
        }
        this.postsService.ltcAddress(data)
            .subscribe(result => {
                if (result) {
                    this.ltcData = result.data;
                    ///this.imgdata = result.data.imageData;
                    // this.scrtKey = result.data.secretKey;
                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }
    getBtc() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        this.postsService.btcAddress(currentUser)
            .subscribe(result => {
                if (result) {
                    this.btcData = result.data;
                    ///this.imgdata = result.data.imageData;
                    // this.scrtKey = result.data.secretKey;
                }
                else {
                    this.error = 'Data Not found';
                }
            });
    }


    // tokencontent(tokenval) {
    //     let btc = "btccontent";
    //     let eth = "ethcontent";
    //     $("#" + tokenval).show();
    //     if (tokenval != btc) {
    //         $("#" + btc).hide();
    //     }
    //     if (tokenval != eth) {
    //         $("#" + eth).hide();
    //     }

    // }
    copyETHURL() {
        let copyText = (<HTMLInputElement>document.getElementById("myETH"));
        copyText.select();
        document.execCommand("Copy");
        // this.dialogService.addDialog(
        //     AlertModalComponent,
        //     {
        //         title: 'Copied',
        //         message: "ETH the address is: " + copyText.value
        //     }
        // )
        alert("ETH the address is: " + copyText.value);
    }

    copyBTCURL() {
        let copyText = (<HTMLInputElement>document.getElementById("myBTC"));
        copyText.select();
        document.execCommand("Copy");
        // alert("Copied BTC the address is: " + copyText.value);
        // this.dialogService.addDialog(
        //     AlertModalComponent,
        //     {
        //         title: 'Copied',
        //         message: "BTC the address is: " + copyText.value
        //     }
        // )
        alert("BTC the address is: " + copyText.value);

    }
    // copyLTCURL() {
    //     let copyText = (<HTMLInputElement>document.getElementById("myLTC"));
    //     copyText.select();
    //     document.execCommand("Copy");
    //     // alert("Copied BTC the address is: " + copyText.value);
    //     this.dialogService.addDialog(
    //         AlertModalComponent,
    //         {
    //             title: 'Copied',
    //             message: "LTC the address is: " + copyText.value
    //         }
    //     )
    // }
    // copyIOTAURL() {
    //     let copyText = (<HTMLInputElement>document.getElementById("myIOTA"));
    //     copyText.select();
    //     document.execCommand("Copy");
    //     // alert("Copied BTC the address is: " + copyText.value);
    //     this.dialogService.addDialog(
    //         AlertModalComponent,
    //         {
    //             title: 'Copied',
    //             message: "IOTA the address is: " + copyText.value
    //         }
    //     )
    // }
    // copyZECURL() {
    //     let copyText = (<HTMLInputElement>document.getElementById("myZEC"));
    //     copyText.select();
    //     document.execCommand("Copy");
    //     // alert("Copied BTC the address is: " + copyText.value);
    //     this.dialogService.addDialog(
    //         AlertModalComponent,
    //         {
    //             title: 'Copied',
    //             message: "ZEC the address is: " + copyText.value
    //         }
    //     )
    // }
    // copyXRPURL() {
    //     let copyText = (<HTMLInputElement>document.getElementById("myXRP"));
    //     copyText.select();
    //     document.execCommand("Copy");
    //     // alert("Copied BTC the address is: " + copyText.value);
    //     this.dialogService.addDialog(
    //         AlertModalComponent,
    //         {
    //             title: 'Copied',
    //             message: "XRP the address is: " + copyText.value
    //         }
    //     )
    // }
    buyBTC() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,
            "alphaxvalue": this.btctoalpha,
            "btcvalue": this.btcBalance,
            "cointype": "BTC",
            "userbtctousd": this.btctousd,
        }
        //  this.spinner.show();
        this.buystatus = false;
        this.postsService.BuywithBTC(data)
            .subscribe(result => {
                if (result) {
                    this.buystatus = true;
                    alert(result.msg);
                    $("#purchaseBTCModalLabel").modal("hide");
                    this.getBalances();
                    this.spinner.hide();
                }
            })
    }
    buyETH() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,
            "alphaxvalue": this.ethtoalphax,
            "ethvalue": this.ethBalance,
            "cointype": "ETH",
            "userethtousd": this.ethtousd,
        }
        // this.spinner.show();
        this.buystatus = false;

        this.postsService.BuywithETH(data)
            .subscribe(result => {

                if (result) {
                    this.buystatus = true;

                    alert(result.msg);
                    $("#purchaseETHModalLabel").modal("hide");
                    this.getBalances();

                    // this.spinner.hide();

                }
            })

    }
    buyBCH() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,
            "alphaxvalue": this.bchtoalphax,
            "bchvalue": this.bchBalance,
            "cointype": "BCH",
            "userbchtousd": this.bchtousd,
        }
        // this.spinner.show();
        this.buystatus = false;
        this.postsService.BuywithBCH(data)
            .subscribe(result => {
                this.buystatus = true;

                if (result) {
                    alert(result.msg);
                    $("#purchaseBCHModalLabel").modal("hide");
                    this.getBalances();

                    // this.spinner.hide();

                }
            })

    }
    buyLTC() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,
            "alphaxvalue": this.ltctoalphax,
            "ltcvalue": this.ltcBalance,
            "cointype": "LTC",
            "userltctousd": this.ltctousd,
        }
        // this.spinner.show();
        this.buystatus = false;
        this.postsService.BuywithLTC(data)
            .subscribe(result => {
                this.buystatus = true;

                if (result) {
                    alert(result.msg);
                    $("#purchaseLTCModalLabel").modal("hide");
                    this.getBalances();


                }
            })

    }
    generateUSD() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,

        }
        this.postsService.generatealphaxvalue(data)
            .subscribe(result => {

                if (result) {
                    this.btctousd = result.userbtctousd.toFixed(2);
                    this.ethtousd = result.userethtousd.toFixed(2);
                    this.bchtousd = result.userbchtousd.toFixed(2);
                    this.ltctousd = result.userltctousd.toFixed(2);
                    this.bchtoalphax = result.bchtoalphax.toFixed(8);
                    this.btctoalpha = result.btctoalpha.toFixed(8);
                    this.ethtoalphax = result.ethtoalphax.toFixed(8);
                    this.ltctoalphax = result.ltctoalphax.toFixed(8);
                    this.slabValue = result.current_slab_value;
                    this.startdate = result.start_date;

                    this.enddate = result.end_date;

                    this.checkLimitedUSD();


                }
            })

    }
    gettokendetails() {

        this.postsService.getsoldtokenDetails()
            .subscribe(result => {
                if (result) {
                    this.tokensold = result.data.TokenSold;
                    this.totaltoken = result.data.totalToken;
                    this.tokenremaining = result.data.TokenRemaining;
                    this.percentage = (this.tokensold / this.totaltoken) * 100;
                }
            })
    }
    minimumBalUSD() {
        // this.loaderService.addLoaderDialog();
        let data = {
            "userName": this.userName,

        }
        this.postsService.getMinimumBalance(data)
            .subscribe(result => {

                if (result) {

                    this.minUSDBTC = result.userbtctousd;
                    this.minUSDETH = result.userbtctousd;
                    this.minUSDBCH = result.userbtctousd;
                    this.minUSDLTC = result.userbtctousd;

                    this.minUSDStatus = result.alphax_min_usd_flag;


                }
            })

    }
    arertcopy() {
        alert("Copied Address Successfully!!!");
    }
    getliveValue() {
        this.postsService.getLive()
            .subscribe(result => {
                if (result) {
                    this.btclive = result.btcval;
                    this.ethlive = result.ethval;
                    this.bchlive = result.bchval;
                    this.ltclive = result.ltcval;
                    this.alphaXlive = result.alphaxval;
                }
            })
    }
    calculator(value, currency) {
        this.selectedCurrency = currency;
        if (value == "" || currency == "" || value == undefined || currency == undefined) {

        }
        else {
            if (currency == "BTC") {
                this.calbtc = value * this.btclive;
                if (this.calbtc != undefined) {
                    this.calUSD = (this.calbtc).toFixed(2);
                    this.calAx = (this.calUSD / this.alphaXlive).toFixed(8);
                }

            }
            else if (currency == "ETH") {
                this.caleth = value * this.ethlive;
                if (this.caleth != undefined) {
                    this.calUSD = (this.caleth).toFixed(2);
                    this.calAx = (this.calUSD / this.alphaXlive).toFixed(8);
                }
            }
            else if (currency == "BCH") {
                this.calbch = value * this.bchlive;
                if (this.calbch != undefined) {
                    this.calUSD = (this.calbch).toFixed(2);
                    this.calAx = (this.calUSD / this.alphaXlive).toFixed(8);
                }
            }
            else {
                this.calltc = value * this.ltclive;
                if (this.calltc != undefined) {
                    this.calUSD = (this.calltc).toFixed(2);
                    this.calAx = (this.calUSD / this.alphaXlive).toFixed(8);
                }

            }


        }








    }



    checkLimitedUSD() {
        if (this.btctousd >= this.slabValue) {
            this.btcstatus = true;
        }
        else {
            this.btcstatus = false;
        }

        if (this.ethtousd >= this.slabValue) {
            this.ethstatus = true;
        }
        else {
            this.ethstatus = false;
        }

        if (this.bchtousd >= this.slabValue) {
            this.bchstatus = true;
        }
        else {
            this.bchstatus = false;
        }

        if (this.ltctousd >= this.slabValue) {
            this.ltcstatus = true;
        }
        else {
            this.ltcstatus = false;
        }
    }
    updateAll() {
        let username = {
            "userName": this.userName
        }
        this.postsService.updateBTCtran(username)
            .subscribe(
                data => {

                    
                },
                error => {
                    // this.showAlert('alertSignin');
                    // this._alertService.error(error);
                });
        this.postsService.updateETHtran(username)
            .subscribe(
                data => {
                   
                },
                error => {
                    // this.showAlert('alertSignin');
                    // this._alertService.error(error);
                });
        this.postsService.updateLTCtran(username)
            .subscribe(
                data => {
                    
                },
                error => {
                    
                });
        this.postsService.updateBCHtran(username)
            .subscribe(
                data => {
                    
                },
                error => {
                    
                });
    }

    refreshBtc(){
        let username = {
            "userName" : this.userName
        }
        this.postsService.updateBTCtran(username)
            .subscribe(
                data => {

                    
                },
                error => {
                    // this.showAlert('alertSignin');
                    // this._alertService.error(error);
                });
    }

    refreshEth(){
        let username = {
            "userName" : this.userName

        }
        this.postsService.updateETHtran(username)
        .subscribe(
            data => {

            },
            error =>{

            }


        );

    }

    refreshLtc(){
        let username = {
            "userName" : this.userName
        }

        this.postsService.updateLTCtran(username)
        .subscribe(
            data =>{

            },
            error =>{

            }
        );
    }

    refreshBch(){
        let username = {
            "userName" : this.userName
        }

        this.postsService.updateBCHtran(username)
        .subscribe(
            data =>{

            },

            error =>{
                
            }


        );
    }

    getKycDetails() {
        let currentUser = JSON.parse(localStorage.getItem('currentUserAlphaX'));
        let name = {
            "username": currentUser.userName,
        }
        this.postsService.kycAlreadyexist(name)
            .subscribe(result => {
                if (result) {
                    this.kycvalue = result.data;
                }
                else {
                    this.kycvalue = false;
                }
      
            });
      }
      checkAlphaxAddress(){
        let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.postsService.checkAndGenerateAlphaxAddress({"username":userName})
        .subscribe(res => {
          if (!res.error) {
            console.log("Try Again");
          }
        })
      }  

      checkBTCAddress(){
        let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.postsService.checkAndGenerateBTCAddress({"username":userName})
        .subscribe(res => {
          if (!res.error) {
            console.log("Try Again");
          }
        })
      }
      checkLTCAddress(){
        let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.postsService.checkAndGenerateLTCAddress({"username":userName})
        .subscribe(res => {
          if (!res.error) {
            console.log("Try Again");
          }
        })
      }
      checkBCHAddress(){
        let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.postsService.checkAndGenerateBCHAddress({"username":userName})
        .subscribe(res => {
          if (!res.error) {
            console.log("Try Again");
          }
        })
      }
      checkETHAddress(){
        let userName = JSON.parse(localStorage.getItem('currentUserAlphaX')).userName;
        this.postsService.checkAndGenerateETHAddress({"username":userName})
        .subscribe(res => {
          if (!res.error) {
            console.log("Try Again");
        }
        })
      }
}
