import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRouteSnapshot, ActivatedRoute, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

import { AppComponent } from '../app.component';

@Component({
  selector: 'app-securitycode',
  templateUrl: './securitycode.component.html',
  styleUrls: ['./securitycode.component.css']
})
export class SecuritycodeComponent implements OnInit {

  constructor(public app: AppComponent, private router: Router, private postsService: PostsService,) { }

  ngOnInit() {
    this.app.isAutherizes  = false;
  }
  token: string = "";

  verifycode() {
    if (this.token != "") {
        let data = {
            "token": this.token
        }
        this.postsService.forgotVerifyCode(data)
            .subscribe(
            data => {
                if (data.status === "OK") {
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         message: data.msg
                    //     })
                     alert(data.msg);
                    this.router.navigate(['/changepassword']);
                } else {
                    // this.dialogService.addDialog(
                    //     AlertModalComponent,
                    //     {
                    //         // title:'Confirm title', 
                    //         message: 'Security code is either invalid or expired.'
                    //     })
                     alert('Security code is either invalid or expired.')
                }
            },
            error => {
                // this.dialogService.addDialog(
                //     AlertModalComponent,
                //     {
                //         message: error
                //     })
                 alert(error);
            });
    } else {
      alert("Enter valid security code");
        // this.dialogService.addDialog(
        //     AlertModalComponent,
        //     {
        //         message: 'Enter valid security code'
        //     })
    }

}
}
